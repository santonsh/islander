# Project:  pyhmmer/islander
# Date:     25/06/2016
# Author:   Anton Shifman [santonsh[at]gmail.com]
# Supervisor: Noam Grinmberg
# File version: 1.0
#
# Description: This file implements island inspection GUI. This GUI can be used 
# to see found islands after a PFAM stage - this way the island are most informative
# and have best annotations.
# Please not that this script can have troubles to inspect especially big island files
# In case of bad performance please use manual island inspection methods using
# output files of intelligently crop these files to fit this scrips performance 



import sys
from PyQt4 import QtGui
from PyQt4 import QtCore
from pyhmmer_io_aux import read_isle_gff_file
from pyhmmer_io_aux import open_run_config

from PIL import Image
import numpy as np
import os

def gene_rep(in_island, identification):
    # This will create single gene color representation

    # Creating image
    gene_width = 3
    bar_height = 10
    dot_height = 5
    dot_y_offset = 6
    bar_y_offset = 4
    space_width = 2
        
    width = gene_width+2
    height = bar_height
    img = QtGui.QImage(width, height+8, QtGui.QImage.Format_RGB32)
    img.fill(QtGui.QColor(55,55,55).rgb())
    
    # Filling the image
    x_cur = 1

    if in_island:
        draw_height = bar_height
        y_offset = bar_y_offset
    else:
        draw_height = dot_height
        y_offset = dot_y_offset
            
    # Identification
    if identification == 'none':
        color = QtGui.QColor(230,230,230).rgb()
    elif identification == 'PFAM':
        color = QtGui.QColor(200,50,200).rgb()
    elif identification == 'HMMER':
        color = QtGui.QColor(0,230,0).rgb()
        
    # Draw a CDS representation
    for x in xrange(gene_width):
        for y in xrange(draw_height):
            img.setPixel(x_cur+x, y_offset+y, color)
            
    # Return pixmap   
    gene_pix = QtGui.QPixmap.fromImage(img)
    return gene_pix

def genome_rep(cds_seq):
    # This will create genome representation in Qt representing island identified genes
    # as green bars, unidentified as white bars and pfam identified as blue bars.
    # Non island genes are represented as white dots
    
    # Filling the image
    i = 0
    #genome_grid = QtGui.QGridLayout()
    genome_grid = QtGui.QHBoxLayout()
    for cds in cds_seq:
        # Reading the CDS
        # Weather in island or not
        in_island = cds[3] == 'True'

        # Mapping
        mapping = cds[2]
        
        # Identification
        if cds[2] == 'none':
            identification = 'none'
        elif cds[2][0:2] == 'PF':
            identification = 'PFAM'
        else:
            identification = 'HMMER'

        # CDS id
        cds_id = cds[1]

        cds_lbl = QtGui.QLabel()
        cds_lbl.setPixmap( gene_rep(in_island, identification) )
        cds_lbl.setToolTip(cds_id+' : '+mapping)
        #genome_grid.addWidget(cds_lbl, 1,i+1)
        genome_grid.addWidget(cds_lbl)
        i = i+1

    genome_grid.addStretch()
    genome_grid.setSpacing(0)

    return genome_grid

def islands_view(islands):

    check_boxes_enabled = False

    isles_grid = QtGui.QGridLayout()

    id_list = []
    src_cb_list = []
    isle_cb_list = []
    graph_rep_list = []

    i = 0
    
    for src in islands.keys():
        #print src
        j = 0
        for island in islands[src]:

            # Add id and basic info
            #print island.keys()
            isl_id_lbl = QtGui.QLabel(island['id']+' [ e='+"{:.4f}".format(float(island['e_val']))+',   weight='+"{:.1f}".format(float(island['weight']))+' ]')
            
            id_list.append(isl_id_lbl)
            
            # Add annotation checkboxes
            if j == 0:
                src_cb_list.append(QtGui.QCheckBox(''))
            else:
                src_cb_list.append(QtGui.QLabel(''))
            isle_cb_list.append(QtGui.QCheckBox(''))

            # Add graphical representation
            
            #graph_rep_lbl = QtGui.QLabel()
            #graph_rep_lbl.setPixmap( genome_rep(island['CDS']) )
            graph_rep_lbl = genome_rep(island['CDS'])
            graph_rep_list.append(graph_rep_lbl)

            # Place the widjets in the grid
            isles_grid.addWidget(id_list[i], i,1)
            if check_boxes_enabled:
                isles_grid.addWidget(src_cb_list[i], i,2)
                isles_grid.addWidget(isle_cb_list[i], i,3)
            isles_grid.addLayout(graph_rep_list[i], i,4)                

            i = i + 1
            j = j + 1

        # Put spaces between sources
        id_list.append(QtGui.QLabel(''))
        graph_rep_list.append(QtGui.QLabel(''))
        src_cb_list.append(QtGui.QLabel(''))
        isle_cb_list.append(QtGui.QLabel(''))
            
        isles_grid.addWidget(id_list[i], i,1)
        if check_boxes_enabled:
            isles_grid.addWidget(src_cb_list[i], i,2)
            isles_grid.addWidget(isle_cb_list[i], i,3)
        isles_grid.addWidget(graph_rep_list[i], i,4)

        i = i + 1

    isles_area = QtGui.QGroupBox()
    isles_area.setLayout(isles_grid)
        
    return isles_area
    
    
class islands_window(QtGui.QDialog):
   
    def __init__(self, parent=None):
        super(islands_window, self).__init__(parent)

        # Initiating island file open button
        self.isl_file = QtGui.QHBoxLayout()
        self.isl_f_name = QtGui.QLineEdit("Please select output dir")
        self.isl_f_btn = QtGui.QPushButton("Open")
        self.isl_file.addWidget(self.isl_f_name)
        self.isl_file.addWidget(self.isl_f_btn)
        self.islands = []

        c = open_run_config('file_dir_config.json')

        def choose_isl_dict():
            self.isl_f_btn.setText('Wait...')
            ret = QtGui.QFileDialog.getExistingDirectory (self, "Choose output dir", "Output")
            
            if (len(ret)>0) and (os.path.exists(os.path.join(os.getcwd(), c['gen_output_d'], str(ret), c['gen_isl_pfam_with_src_gff_f']))):
                self.isl_f_name.setText(ret)

                isl_file = os.path.join(os.getcwd(), c['gen_output_d'], str(ret), c['gen_isl_pfam_with_src_gff_f'])
                            
                islands = read_isle_gff_file(isl_file)
                isl_num = 0
                for src in islands.keys():
                    isl_num = isl_num + len(islands[src])
                #print isl_num
                if isl_num<1:
                    text = 'No islands found in the annotation file in this directory. Please try again'
                    self.isl_f_name.setStyleSheet("color: rgb(255, 0, 0);")
                else:
                    text = 'Cool. Found ' + str(isl_num) + ' islands in the output directory'
                    self.isl_f_name.setStyleSheet("color: rgb(0, 180, 0);")
                    self.isles_scroll.setWidget(islands_view(islands))
                print text
            self.isl_f_btn.setText('Open')
            
            #show_islands()

        self.connect(self.isl_f_btn, QtCore.SIGNAL("clicked()"), choose_isl_dict)

        # Initiate islands area
        isles_area = QtGui.QGroupBox()
        
        self.isles_scroll = QtGui.QScrollArea()
        self.isles_scroll.setWidget(isles_area)
        self.isles_scroll.setWidgetResizable(True)
        self.isles_scroll.setFixedHeight(300)
        self.isles_scroll.setFixedWidth(900)
        


        # Adding buttons
        self.btns_area = QtGui.QHBoxLayout()
        self.check_all_btn = QtGui.QPushButton("Check all")
        self.uncheck_all_btn = QtGui.QPushButton("Uncheck all")
        self.annotate_btn = QtGui.QPushButton("Annotate selected")
        self.btns_area.addWidget(self.check_all_btn)
        self.btns_area.addWidget(self.uncheck_all_btn)
        self.btns_area.addWidget(self.annotate_btn)

        # Button functionality
        def check_all_check_boxes():
            for cb in src_cb_list+isle_cb_list:
                if type(cb) == type(QtGui.QCheckBox()):
                    cb.setChecked(True)

        def uncheck_all_check_boxes():
            for cb in src_cb_list+isle_cb_list:
                if type(cb) == type(QtGui.QCheckBox()):
                    cb.setChecked(False)

        def annotate_selected_islands():
            # Check the checkboxes - what to annotate
            isle_num_to_annotate_list = []
            i = 0
            for cb in isle_cb_list:
                if type(cb) == type(QtGui.QCheckBox()) and cb.isChecked():
                    isle_num_to_annotate_list.append(i)
                    i=i+1
            print isle_num_to_annotate_list
            # Choose the islands to annotate from the big dictionary
            i = 0
            for_annotation = {}
            for src in gff_islands.keys():
                for island in gff_islands[src]:
                    if i in isle_num_to_annotate_list:
                        if not src in for_annotation:
                            for_annotation[src] = []
                        for_annotation[src].append(island)
                    i = i + 1

            print for_annotation



        self.connect(self.check_all_btn, QtCore.SIGNAL("clicked()"), check_all_check_boxes)
        self.connect(self.uncheck_all_btn, QtCore.SIGNAL("clicked()"), uncheck_all_check_boxes)
        self.connect(self.annotate_btn, QtCore.SIGNAL("clicked()"), annotate_selected_islands)



        # Combine all the parts of the layout
        
        layout = QtGui.QVBoxLayout(self)
        layout.addLayout(self.isl_file)
        layout.addWidget(self.isles_scroll)
        #layout.addLayout(self.btns_area)





if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    
    main = islands_window()
    main.show()

    sys.exit(app.exec_())
