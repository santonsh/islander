# Project:  pyhmmer/islander
# Date:     25/06/2016
# Author:   Anton Shifman [santonsh[at]gmail.com]
# Supervisor: Noam Grinmberg
# File version: 1.0
#
# Description: This file main auxiliary library for scripts of pyhmmer/islander
# In this file one can find the functions used in different script most notably
# the 'stages' script 


from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.Blast import NCBIXML
#from BCBio.GFF import GFFExaminer
from BCBio import GFF
#from itertools import tee

import re
import json
import os



# # # # # # # # # # # # # # # # # # #
#
# This section contains functions
# related to configuration stage and
# GUI
#
# # # # # # # # # # # # # # # # # # #
           


def auto_detect_file(directory, extension):
    ''' This function autodetects specific files in specific directory
    This is used in gui and auto configuration scripts

    Input
    
    @directory - a directory to look for a file in

    @extension - a file extension to look for

    Output
    
    @return - a full filename (string) in case a single file with matching extension
    was found and an empty string - '' if none or more than one file was found
    '''
    
    # This function autodetects files in directory
    found_files = []
    for root, dirs, files in os.walk(directory, topdown=False):
            for name in files:
                if name.lower().endswith(extension.lower()):
                    found_files.append(os.path.join(root, name))

    if len(found_files) == 0:
        print 'No '+extension.lower()+' file was found. Please check the metagenome directory'
        file_name = ''
    elif len(found_files) > 1:
        print 'More than one '+extension.lower()+' file was found. Please check the metagenome directory and unify the faa files if necessary'
        file_name = ''
    else:
        file_name = found_files[0]
        #print extension+' file found'
        
    return file_name



def hmm_profiles_number(file_name):
    ''' This function will return a number of hmm profiles in hmm profile file
    This is used in gui to check hmm profile file

    Input
    
    @file_name - hmm profiles file to investigate

    Output
    
    @return - an integer representing the number of HMM profiles found in HMM
    profiles file
    '''

    profiles = []
    with open(file_name, 'r') as f:
        profiles = re.findall('.*NAME\s(.*)\n', f.read())

    if not profiles:
        return 0 
    else:
        return len(profiles)


    
def examine_gff(file_name):
    ''' This function returns dictionary of meta sequence lengths (in CDS terms)
    This is used in gui to check meta-sequence source GFF file

    Input
    
    @file_name - meta-sequence GFF file to investigate

    Output
    
    @return - a dictionary of meta-sequence length's (in CDS terms).
    The dictionary will have a meta-sequence names as keys and a number
    of CDS corresponding to this meta-sequence as a value. If fails for
    some reason to read GFF file it returns empty dictionary - {}
    '''

    try:
        examiner = GFF.GFFExaminer()
        #print file_name
        with open(file_name) as gff_f_hnd:
            return examiner.available_limits(gff_f_hnd)['gff_id']

    except:
        return {}


    
def get_metaseq_fasta_size(file_name):
    ''' This function returns number of records (CDS) in FASTA file
    This is used in gui and auto configuration scripts to check meta-sequence
    FASTA file

    Input
    
    @file_name - FASTA file to investigate

    Output
    
    @return - an integer representing the number of CDS in FASTA file.
    If FASTA file can not bo open for some reason the function will return -1
    '''

    try:
        with open(file_name, "rU") as fasta_f:
            return len(list(SeqIO.parse(fasta_f, "fasta")))
    except:
        return -1


    
def read_hmm_weights(file_name):
    ''' This function returns hmm profile weights from hmm profile weights
    file in a form of dictionary
    This is used in gui to check HMM profile weights file and to load
    the weights in island identification stage

    Input
    
    @file_name - meta-sequence GFF file to investigate

    Output
    
    @return - a dictionary of HMM profile weights.
    The dictionary will have HMM profile names as keys and their corresponding
    weights as a value. 
    '''

    weights = {}
    with open(file_name, 'r') as f:
        for line in f:
            # The line is not commented out
            if len(re.findall('\s*#.*\n', line)) == 0:
                weight_pair = re.findall('(\S+)\s+([0-9\.]+)\n', line)
                # Right weight pair is read
                if len(weight_pair) == 1:
                    weight_pair=weight_pair[0]
                    if len(weight_pair) == 2:
                        weights[weight_pair[0]] = float(weight_pair[1])
    return weights



def open_run_config(config_filename):
    ''' This function is a simple wrapper for reading json configuration file

    Input
    
    @config_filename - configuration json file name

    Output
    
    @return - a configuration dictionary
    '''

    with open(config_filename) as config_f:    
        config = json.load(config_f)

    return config













        












# # # # # # # # # # # # # # # # # # #
#
# This section contains functions
# related to mapping and translation
# stages, hmmerscan and pfanscan 
# output filetering
#
# # # # # # # # # # # # # # # # # # #



def map_cds_features_to_profiles(feature_seq, mapping1={}, marker1=1, mapping2={}, marker2=2):
    '''This function maps the feature sequence to hmm/other profiles

    Input
    
    @feature_seq - a sequence of features to translate for example -
    [fet_0012, fet_001234, fet_23]

    @mapping1 - mapping of first group of profiles for example - 
    {'fet_0012':'PG1_1', 'fet_001234':'PG1_2', ... }

    @marker1 - marker to be used for identified members of first group of profiles

    @mapping2 - mapping of second group of profiles

    @marker2 - marker to be used for identified members of second group of profiles

    Output
    
    @returns - a tuple of (translated_seq, binary_representation) where
    translated_seq is a sequence whith profile names instead of features and
    none for unidentified features like -
    [none, PG1_1, PG1_101, none, PG2_218]
    binary_representation - a representation of the translation like
    [0,1,1,0,2] where 0 is unidentified feature, 1 is a feature identified
    as a member of the first group and 2 is a feature identified as a member of
    the second group

    Note: the translation of the first group is prioritized relative to second
    meaning only unidentified as a first group profiles can be idenified as a second
    group profiles
    '''

    translated_seq = []
    binary_representation = []
    keys1 = mapping1.keys()
    keys2 = mapping2.keys()
    for feature in feature_seq:
        if feature[0] in keys1:
            translated_seq.append(mapping1[feature[0]])
            binary_representation.append(int(marker1))
        elif feature[0] in keys2:
            translated_seq.append(mapping2[feature[0]])
            binary_representation.append(int(marker2))
        else:
            translated_seq.append('none')
            binary_representation.append(0)

    return (translated_seq, binary_representation)



def filter_hmmerscan_domtlb_out(out_f_name, out_filt_f_name):
    ''' This function will filter hmmerscan domtbl output file so it can be used
    as a CDS ---> HMM profile mapping.
    This is used in HMMER run stage.

    This will filter hmmerscan domtbl output in the next way
    1. Delete duplicate mappings - decide on hmm profiles based on e-value
    2. Filter out mappings with low e-value as in PFAM scan
    (if alignment > 80aa, use E-value < 1e-5,
    otherwise use E-value < 1e-3;
    covered fraction of HMM > 0.3)

    Input
    
    @out_f_name - file name of hmmerscan domtbl output file

    @out_filt_f_name - file name of filtered hmmerscan output file to produce

    Assumptions/Paramenters
    (if alignment > 80aa, use E-value < 1e-5,
    otherwise use E-value < 1e-3;
    covered fraction of HMM > 0.3)
    '''

    #from collections import namedtuple
    #mapping = namedtuple('mapping', ('HMMprofile', 'seq', 'tlen', 'qlen', 'ieval', 'hmm_s', 'hmm_e', 'alg_s', 'alg_e'))
    
    with open(out_f_name, 'r') as out_f:
        out_f.next()  # skip header
        out_f.next()  # skip header
        out_f.next()  # skip header
        data = []
        # Read and sort data by sequence name, aligment start and alignment end 
        for line in out_f:
            l = line.split()
            if len(l) == 23:
                #data.append(mapping(HMMprofile=l[0], seq = l[3], tlen = l[2], qlen = l[5], ieval = float(l[12]), hmm_s = int(l[15]), hmm_e = int(l[16]), alg_s =int(l[17]), alg_e = int(l[18])))
                data.append((l[0], l[3], l[2], l[5], float(l[12]), int(l[15]), int(l[16]), int(l[17]), int(l[18])))

        #data.sort(key=lambda s:(s.seq,s.ieval))
        data.sort(key=lambda s:(s[1],s[4]))
        # Remove duplicate mappings based on e-value (based on previous sort)
        # and remove mappings under threshold
        data_filt = []
        prev_seq = []
##        for d in data:
##            if prev_seq != d.seq:
##                prev_seq = d.seq
##                # Check treshold and add the filtered values
##                if (d.alg_e-d.alg_s) > 80:
##                    if d.ieval < 1e-5:        
##                        data_filt.append(d)
##                elif d.ieval < 1e-3:
##                    data_filt.append(d)
        for d in data:
            if prev_seq != d[1]:
                prev_seq = d[1]
                # Check treshold and add the filtered values
                if (d[8]-d[7]) > 80:
                    if d[4] < 1e-5:        
                        data_filt.append(d)
                elif d[4] < 1e-3:
                    data_filt.append(d)
                    
    # write out filtered output                
    with open(out_filt_f_name, 'w') as out_filt_f:
        for d in data_filt:
            line = '\t'.join(str(i) for i in d)
            out_filt_f.write(line+'\n')



def read_filtered_mapping(out_filt_f_name):
    ''' This function will read hmmerscan filtered output file and return it
    in a dictionary form
    This is used in island identification run stage.

    Input

    @out_filt_f_name - file name of filtered hmmerscan output file to read

    Output

    @returns - a dictionary with CDS fasta tags as keys and HMM profiles as values
    '''
    # This will read the filtered hmm mappings to the dictionary

    with open(out_filt_f_name, 'r') as out_f:

        mapping = {}
        # Read and sort data by sequence name, aligment start and alignment end 
        for line in out_f:
            l = line.split()
            if len(l) == 9:
                mapping[l[1]]=l[0]
        return mapping


    
def filter_pfamscan_out(out_f_name, out_filt_f_name):
    ''' This function will filter pfamscan 'non -as' output file so it can be
    used as a CDS ---> PFAM profile mapping.
    This is used in PFAM run stage.

    This will filter pfamscan output in the next way
    1. Delete duplicate mappings - decide on pfam profiles based on e-value
    2. Filter out mappings with low e-value
    (if alignment > 80aa, use E-value < 1e-5,
    otherwise use E-value < 1e-3;
    covered fraction of PFAM > 0.3)

    Input
    
    @out_f_name - file name of hmmerscan domtbl output file

    @out_filt_f_name - file name of filtered hmmerscan output file to produce

    Assumptions/Paramenters
    (if alignment > 80aa, use E-value < 1e-5,
    otherwise use E-value < 1e-3;
    covered fraction of PFAM > 0.3)
    '''

    data = []
    with open(out_f_name, 'r') as out_f:
        # Read and sort data by sequence name, aligment start and alignment end 
        for line in out_f:
            l = line.split()
            if len(l) == 15 and line[0] != '#':
                data.append(l)

        # Sort first by sequence name and then by e-value (to filter out the best later)
        data.sort(key=lambda s:(s[0],float(s[12])))
        # Remove duplicate mappings based on e-value (based on previous sort)
        # and remove mappings under threshold
        data_filt = []
        prev_seq = []
        for d in data:
            if prev_seq != d[0]:
                prev_seq = d[0]
                # Check treshold and add the filtered values
                if (int(d[2])-int(d[1])) > 80:
                    if float(d[12]) < 1e-5:        
                        data_filt.append(d)
                elif float(d[12]) < 1e-3:
                    data_filt.append(d)               
    # write out filtered output                
    with open(out_filt_f_name, 'w') as out_filt_f:
        for d in data_filt:
            line = '\t'.join(str(i) for i in d)
            out_filt_f.write(line+'\n')

            

def read_pfam_filtered_mapping(out_filt_f_name):
    ''' This function will read pfamscan filtered output file and return it
    in a dictionary form
    This is used in the end of PFAM run stage.

    Input

    @out_filt_f_name - file name of filtered pfamscan output file to read

    Output

    @returns - a dictionary with CDS fasta tags as keys and PFAM profiles as values
    '''

    with open(out_filt_f_name, 'r') as out_f:

        mapping = {}
        for line in out_f:
            l = line.split()
            if len(l) == 15:
                mapping[l[0]]=l[5]
        return mapping



# # # # # # # # # # # # # # # # # # #
#
# This section contains functions
# related to island annotation 
# and reading 
#
# # # # # # # # # # # # # # # # # # #



def annotate_isles_to_tbl(islands, isl_out_file_name):
    ''' This function write island dictionary to tbl file

    This function gets the tbl file name and writes to it an island dictionary
    in table form with line for each island

    Input

    @isl_tbl_file - island table file to write to
    
    @islands - a dictionary that has a meta-sequences as a keys and island lists as
    values (as each sequence can have several islands in it).
    Each island in the list then composed of list of the next values:
    source sequence, start CDS, end CDS, length (in CDS terms), e-value, weight
    example of island list:
    SRC00001    12    14    3    0.1    12
    '''
    
    with open(isl_out_file_name, 'w') as out_f:
        out_f.write('Source, Start CDS, End CDS, CDS length, E value of island, Weight of island\n')        
        for src in islands.keys():
            for island in islands[src]:
                # All position info is in CDS not in basepairs
                o_source = src
                o_start = str(island[0])
                o_end = str(island[1])
                o_len = str(island[2])
                o_eval = "{:10.4f}".format(float(island[3]))
                o_wght = "{:10.2f}".format(float(island[4]))

                out_f.write(o_source+'\t'+o_start+'\t'+o_end+'\t'+o_len+'\t'+o_eval+'\t'+o_wght+'\n')



def read_isle_tbl_file(isl_tbl_file):
    ''' This function reads islands from tbl file

    This function gets the tbl file name and returns islands written
    in it in a dictionary form.

    Input

    @isl_tbl_file - island table file to read

    Output
    
    @return - a dictionary that has a meta-sequences as a keys and island lists as
    values (as each sequence can have several islands in it).
    Each island in the list then composed of list of the next values:
    source sequence, start CDS, end CDS, length (in CDS terms), e-value, weight
    example of island list:
    SRC00001    12    14    3    0.1    12
    '''

    out_isle_dict = {}
    
    def skip_comments(f):
        for line in f:
            if not line.strip().startswith('#'):
               yield line
               
    with open(isl_tbl_file, 'r') as tbl_f:
        for line in skip_comments(tbl_f):
            wlist = re.findall('(.+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+\.\d*)\s+(\d+\.\d*)', line)
            # Check the parsed line 
            if len(wlist) == 1 and len(wlist[0]) == 6:
                wlist = wlist[0]
                src = wlist[0]
                start = wlist[1]
                end = wlist[2]
                length = wlist[3]
                e_val = wlist[4]
                weight = wlist[5]

                if src not in out_isle_dict.keys():
                    out_isle_dict[src] = []
                # append new island
                out_isle_dict[src].append([int(start), int(end), int(length), float(e_val), float(weight)])

    return out_isle_dict



def annotate_isles_to_gff(islands, src_dict, src_dict_translated, src_seq_translated_weights, gff_out_file):
    ''' Annotate islands to gff

    This function will need the islands dictionary, metasequence src dictionary,
    translated metasequence src dictionary and gff file name for output.
    The function will annotate the islands as a main feature and their composing
    CDS as their subfeatures containing besides location info such data as
    CDS translation to HMMER/PFAM and CDS weight

    Input

    @islands - islands dictionary

    @src_dict - source meta-sequence dictionary containing CDS fasta tags -
    read metagenome source GFF file basicaly

    @src_dict_translated - thanslation dictionary containing CDS mappings to HMM/PFAM

    @src_seq_translated_weights - CDS weights dictionary (produced in other places
    from HMM weights dictionary)

    @gff_out_file - a file name of island GFF file to write
    '''

    # Go through meta sources containing islands
    records = []
    for meta_src in islands.keys():
        i = 0
        for isl in islands[meta_src]:
            i = i+1
            isl_ID = meta_src+"_"+str(i)
            isl_src = meta_src
            isl_s_cds = isl[0]
            isl_e_cds = isl[1]
            isl_e_val = isl[3]
            isl_wght = isl[4]

            # Preparing island annotation
            seq = Seq("")
            rec = SeqRecord(seq, isl_ID)
           
            start = src_dict[meta_src][isl_s_cds][1]
            
            end = src_dict[meta_src][isl_e_cds][2]
                
            qualifiers = {"source": "islander", "src_meta_seq": isl_src, "CDS_s":isl_s_cds, "CDS_e":isl_e_cds, "isl_eval": isl_e_val, "ID": isl_ID, "Weight": isl_wght}
            island_feature = SeqFeature(FeatureLocation(start, end), type="ISL", strand=None,
                                     qualifiers=qualifiers)

            # Preparing island contents annotation - sequences
            island_feature.sub_features = []
            for j in range(isl_s_cds, isl_e_cds+1):
                cds_name = src_dict[meta_src][j][0]
                start = src_dict[meta_src][j][1]
                end = src_dict[meta_src][j][2]
                
                strand = src_dict[meta_src][j][3]
                if strand == '1' or strand == '+1' or strand == '+':
                    strand = 1
                elif strand == '-1' or strand == '-':
                    strand = -1
                    
                cds_mapping = src_dict_translated[meta_src][j]
                cds_weight = src_seq_translated_weights[meta_src][j]
                in_island = 'True'
                cds_feature = SeqFeature(FeatureLocation(start, end), type="CDS", strand=strand,
                            qualifiers={"source": "islander", "faa_tag": cds_name, "mapping": cds_mapping, "in_island": in_island, "weight": cds_weight})
                island_feature.sub_features.append(cds_feature)    

            rec.features.append(island_feature)

            records.append(rec)

    # Finally write the output GFF
    with open(gff_out_file, "w") as out_handle:
        GFF.write(records, out_handle)



def annotate_isles_incl_src_to_gff(islands, src_dict, src_dict_translated, src_seq_translated_weights, gff_out_file):
    ''' Annotate islands to gff including all source CDS.
    This is kind of annotation is needed mainly for island graphical
    representation script

    This function will need the islands dictionary, metasequence src dictionary,
    translated metasequence src dictionary and gff file name for output.
    The function will annotate the islands as a main feature and all the CDS
    composing  its source meta-sequence as their subfeatures containing besides
    location info such data as CDS translation to HMMER/PFAM and CDS weight

    Input

    @islands - islands dictionary

    @src_dict - source meta-sequence dictionary containing CDS fasta tags -
    read metagenome source GFF file basicaly

    @src_dict_translated - thanslation dictionary containing CDS mappings to HMM/PFAM

    @src_seq_translated_weights - CDS weights dictionary (produced in other places
    from HMM weights dictionary)

    @gff_out_file - a file name of island GFF file to write
    '''

    # Go through meta sources containing islands
    records = []
    for meta_src in islands.keys():
        i = 0
        for isl in islands[meta_src]:
            i = i+1
            isl_ID = meta_src+"_"+str(i)
            isl_src = meta_src
            isl_s_cds = isl[0]
            isl_e_cds = isl[1]
            isl_e_val = isl[3]
            isl_wght = isl[4]

            # Preparing island annotation
            seq = Seq("")
            rec = SeqRecord(seq, isl_ID)
            start = src_dict[meta_src][isl_s_cds][1]
            end = src_dict[meta_src][isl_e_cds][2]
            qualifiers = {"source": "islander", "src_meta_seq": isl_src, "CDS_s":isl_s_cds, "CDS_e":isl_e_cds, "isl_eval": isl_e_val, "ID": isl_ID, "Weight": isl_wght}
            island_feature = SeqFeature(FeatureLocation(start, end), type="ISL", strand=None,
                                     qualifiers=qualifiers)

            # Preparing island contents annotation - sequences
            island_feature.sub_features = []
            src_len = len(src_dict[meta_src])
            #for j in range(isl_s_cds, isl_e_cds+1):
            #print 's', str(0), 'e' , str(src_len+1)
            for j in range(0, src_len):
                #print j
                if (j <= isl_e_cds) and (j>= isl_s_cds):
                    in_island = True
                else:
                    in_island = False
                    
                cds_name = src_dict[meta_src][j][0]
                start = src_dict[meta_src][j][1]
                end = src_dict[meta_src][j][2]
                
                strand = src_dict[meta_src][j][3]
                if strand == '1' or strand == '+1' or strand == '+':
                    strand = 1
                elif strand == '-1' or strand == '-':
                    strand = -1

                cds_mapping = src_dict_translated[meta_src][j]
                cds_weight = src_seq_translated_weights[meta_src][j]
                cds_feature = SeqFeature(FeatureLocation(start, end), type="CDS", strand=strand,
                            qualifiers={"source": "islander", "faa_tag": cds_name, "mapping": cds_mapping, "in_island": in_island, "weight": cds_weight})
                island_feature.sub_features.append(cds_feature)    

            rec.features.append(island_feature)

            records.append(rec)

    # Finally write the output GFF
    with open(gff_out_file, "w") as out_handle:
        GFF.write(records, out_handle)


def read_isle_gff_file(isl_gff_f):
    ''' Read islands gff file

    This function will read the islands GFF file and return an island dictionary

    need the islands dictionary, metasequence src dictionary,
    translated metasequence src dictionary and gff file name for output.
    The function will annotate the islands as a main feature and all the CDS
    composing  its source meta-sequence as their subfeatures containing besides
    location info such data as CDS translation to HMMER/PFAM and CDS weight

    Input

    @isl_gff_f - a file name of island GFF file to read

    Output

    @returns - a dictionary that has a meta-sequences as a keys and island lists as
    values (as each sequence can have several islands in it).
    The example dictionary will look something like this:
    islands ={
            'META_SRC_1': [
            
                        {'src':xxx
                        'CDS_s':1
                        'CDS_e':10
                        'len':2
                        'e_val':0.1
                        'weight':12
                        'CDS': [[bla],[bla],[bla]]
                        },
                        
                        {'src':xxx
                        'CDS_s':1
                        'CDS_e':10
                        'len':2
                        'e_val':0.1
                        'weight':12
                        'CDS': [[bla],[bla],[bla]]
                        }

                        .
                        .
                        .
                        
                        ],
            .
            .
            .
            
             }
    '''

    # Prepare CDS for filtered output
    cds_for_filt_out = []

    # Discover all the islands
    isles = {}
    with open(isl_gff_f) as isl_gff_hnd:
        for rec in GFF.parse(isl_gff_hnd, limit_info=dict(gff_type = ['ISL'])):
            isle_id = rec.features[0].id
            src = rec.features[0].qualifiers['src_meta_seq'][0]
            CDS_e = int(rec.features[0].qualifiers['CDS_e'][0])
            CDS_s = int(rec.features[0].qualifiers['CDS_s'][0])
            e_val = float(rec.features[0].qualifiers['isl_eval'][0])
            weight = float(rec.features[0].qualifiers['Weight'][0])

            if isle_id not in isles.keys():
                isles[isle_id] = {}
            isles[isle_id]['src'] = src
            isles[isle_id]['CDS'] = []
            isles[isle_id]['CDS_s'] = CDS_s
            isles[isle_id]['CDS_e'] = CDS_e
            isles[isle_id]['len'] = CDS_e-CDS_s
            isles[isle_id]['e_val'] = e_val
            isles[isle_id]['weight'] = weight
            
    # Add the CDS to islands
    isle_ids = isles.keys()
    #print isle_ids
    with open(isl_gff_f) as isl_gff_hnd:
        for rec in GFF.parse(isl_gff_hnd, limit_info=dict(gff_type = ['CDS'])):
            for cds in rec.features[0].sub_features:
                strand = cds.location.strand
                isle_id = cds.qualifiers['Parent'][0]
                faa_tag = cds.qualifiers['faa_tag'][0]
                mapping = cds.qualifiers['mapping'][0]
                weight = cds.qualifiers['weight'][0]
                try:
                    in_island = cds.qualifiers['in_island'][0]
                except:
                    in_island = True
                isles[isle_id]['CDS'].append([strand, faa_tag, mapping, in_island, weight])

    # Translate islands dictionary to source based form
    isles_dict = {}
    for isl_id in isles.keys():
        isl = isles[isl_id]
        src = isl['src']
        if src not in isles_dict.keys():
            isles_dict[src] = []
        isl_to_add = {}
        isl_to_add['id'] = isl_id
        isl_to_add['CDS_s'] = isl['CDS_s']
        isl_to_add['CDS_e'] = isl['CDS_e']
        isl_to_add['len'] = isl['len']
        isl_to_add['e_val'] = isl['e_val']
        isl_to_add['CDS'] = isl['CDS']
        isl_to_add['weight'] = isl['weight']
        isles_dict[src].append(isl_to_add)
        
    return isles_dict



# # # # # # # # # # # # # # # # # # #
#
# This section contains functions
# related to CDS in/out island
# frequency analysis 
#
# # # # # # # # # # # # # # # # # # #



def extract_cds_from_isl_gff(isl_gff_f):
    ''' Reads islands gff file and extracts from it all the CDS of all the found islands
    This is used in the last common stage to identify all the in-island CDS of
    big metagenome

    Input

    @isl_gff_f - a file name of island GFF file to read and analize

    Output

    @returns - a list of CDS contained in islands within island GFF file
    '''

    # Prepare CDS list
    cds_list = []
            
    # Go through CDS records
    with open(isl_gff_f) as isl_gff_hnd:
        for rec in GFF.parse(isl_gff_hnd, limit_info=dict(gff_type = ['CDS'])):
            for cds in rec.features[0].sub_features:
                faa_tag = cds.qualifiers['faa_tag'][0]
                try:
                    in_island = cds.qualifiers['in_island'][0]
                except:
                    in_island = True
                cds_list.append(faa_tag)
        
    return cds_list



def annotate_unidentified_cds_to_tbl_file(isl_gff_f, meta_src, unid_out_f, in_island_frequency):
    ''' This function annotates in-island unidentified CDS to tbl file

    Input

    @isl_gff_f - island GFF file for CDS info reference

    @meta_src - source metagenome folder name for the record

    @unid_out_f - table file to write to

    @in_island_frequency - a dictionary with in-out-island frequency data for key CDS
    
    Note
    The example line of the output table file look like this:
    GBSCSSed77CDRAFT_000001363	300000085	GBSCSSed77CDRAFT_c000001	GBSCSSed77CDRAFT_c000001_1	177	0.01510	31.00	[332403:334613](-)		736	1	6	1	3
    '''

    # Read the islands data
    isles = {}
    with open(isl_gff_f) as isl_gff_hnd:
        for rec in GFF.parse(isl_gff_hnd, limit_info=dict(gff_type = ['ISL'])):
            isle_id = rec.features[0].id
            src = rec.features[0].qualifiers['src_meta_seq'][0]
            CDS_e = int(rec.features[0].qualifiers['CDS_e'][0])
            CDS_s = int(rec.features[0].qualifiers['CDS_s'][0])
            e_val = float(rec.features[0].qualifiers['isl_eval'][0])
            weight = float(rec.features[0].qualifiers['Weight'][0])

            if isle_id not in isles.keys():
                isles[isle_id] = {}
            isles[isle_id]['src'] = src
            isles[isle_id]['CDS'] = []
            isles[isle_id]['CDS_s'] = CDS_s
            isles[isle_id]['CDS_e'] = CDS_e
            isles[isle_id]['len'] = CDS_e-CDS_s
            isles[isle_id]['e_val'] = e_val
            isles[isle_id]['weight'] = weight

    # Read the CDS data and write to file
    with open(unid_out_f, 'w') as out_f:

        # The headers line
        first_line = '### cds_id, metagenome, src_seq, src_isl, isl_len(CDS), isl_eval, isl_wght, CDS location, CDS len (amino), in_island_blast_hits_num, out_island_blast_hits_num, in_island_blast_long_hits_num, out_island_blast_long_hits_num'
        out_f.write(first_line+'\n')

        with open(isl_gff_f) as isl_gff_hnd:
            # Go through all CDS
            for rec in GFF.parse(isl_gff_hnd, limit_info=dict(gff_type = ['CDS'])):
                for cds in rec.features[0].sub_features:
                    mapping = cds.qualifiers['mapping'][0]
                    try:
                        in_island = cds.qualifiers['in_island'][0]
                    except:
                        in_island = 'True'

                    if in_island == 'True' and mapping == 'none':  

                        CDS_location = cds.location
                        isl_id = cds.qualifiers['Parent'][0]
                        CDS_id = cds.qualifiers['faa_tag'][0]

                        isl_len = isles[isl_id]['len']
                        isl_eval = isles[isl_id]['e_val']
                        isl_wght = isles[isl_id]['weight']
                        src_seq = isles[isl_id]['src']
                        
                        
                        src_str =  '\t{:s}\t{:s}\t{:s}\t{:s}'.format(CDS_id, meta_src, src_seq, isl_id)
                        data_str = '\t{:d}\t{:.4f}\t{:.1f}\t{:s}'.format(isl_len, isl_eval, isl_wght, CDS_location)
                        freq_str = '\t{:d}\t{:d}\t{:d}\t{:d}\t{:d}'.format(in_island_frequency[CDS_id]['len'],in_island_frequency[CDS_id]['in_isl'],in_island_frequency[CDS_id]['out_isl'],in_island_frequency[CDS_id]['in_isl_l'],in_island_frequency[CDS_id]['out_isl_l'])

                        new_line = src_str+'\t'+data_str+'\t'+freq_str
                        
                        out_f.write(new_line+'\n')



def analyze_island_blast_hits_frequency(blast_out_f, in_island_CDS_lisf_f):
    ''' This function analizes in/out island hit frequency of  

    Input
    
    @blast_out_f - BLAST output file to count query CDS hits

    @in_island_CDS_lisf_f - a file containing in-island CDS list
    
    Output

    @returns - a dictionary with BLAST query CDS as keys and a frequency
    info dictionary as values
    '''
    
    freq_data ={}

    # Reading in-island CDS list
    with open(in_island_CDS_lisf_f, 'r') as in_island_CDS_list_f_hnd:
        in_isl_CDS_list = in_island_CDS_list_f_hnd.read().splitlines()
        
        from sets import Set
        in_isl_CDS_list_lower = Set()
        
        for cds in in_isl_CDS_list:
            in_isl_CDS_list_lower.add(cds.lower())

    # Reading BLAST output
    with open(blast_out_f, 'r') as blast_out_f_hnd:
        blast_records = NCBIXML.parse(blast_out_f_hnd)

        # Go through queries
        for rec in blast_records:
            query_CDS = rec.query
            query_CDS_len = rec.query_length
            in_isl_hits = 0
            out_isl_hits = 0
            in_isl_long_hits = 0
            out_isl_long_hits = 0
            
            # Go through hits
            for alignment in rec.alignments:
                hit_id = alignment.hit_id
                hit_len = alignment.length

                # Count in/out island hit number
                if hit_id.lower() in in_isl_CDS_list_lower:
                    in_isl_hits = in_isl_hits+1
                    if float(hit_len)/query_CDS_len > 0.7:
                        in_isl_long_hits = in_isl_long_hits+1
                else:
                    out_isl_hits = out_isl_hits+1
                    if float(hit_len)/query_CDS_len > 0.7:
                        out_isl_long_hits = out_isl_long_hits+1

            # Add a record to frequency data dictionary
            freq_data[query_CDS] = {'in_isl'    :in_isl_hits,
                                    'out_isl'   :out_isl_hits,
                                    'in_isl_l'  :in_isl_long_hits,
                                    'out_isl_l' :out_isl_long_hits,
                                    'len'       :query_CDS_len
                                    }
    
    return freq_data



# # # # # # # # # # # # # # # # # # #
#
# This section contains functions
# related to various stages 
#
# # # # # # # # # # # # # # # # # # #

def create_combined_fasta_with_island_related_unidentified_CDS(full_unidentified_blast_fasta_f, output_d, partial_blast_fasta_f_name):
    ''' This function creates a tree file from a fasta file

    '''

##
##        SeqRec_out_list = []
##    
##    full_fasta_dict = SeqIO.index(in_fasta, "fasta")
##    for cds in included_cds:
##        try:
##            SeqRec_out_list.append(full_fasta_dict[cds]) 
##        except:
##            print '\t',cds, 'was not found in the fasta file'
##    with open(out_fasta, "w") as out_fasta_f:
##        SeqIO.write(SeqRec_out_list, out_fasta_f, "fasta")
##
##    print '\tFiltering produced', len(SeqRec_out_list), 'CDS seqs out of initial', len(full_fasta_dict), 'CDS seqs'
##
##    return len(SeqRec_out_list)
##



    #print '\n\tCreating common FASTA file for island unidentified CDS...'
    #start = time.time()

    # Creating full FASTA file consisting of unidentified CDS
    SeqRec_out_list = []
    in_IDs = []

    files = os.listdir(output_d)
    i = 0
    for directory in files:
        part_faa_f = os.path.join(os.getcwd(), output_d, directory, partial_blast_fasta_f_name)
        if os.path.exists(part_faa_f):
            i = i + 1
            with open(part_faa_f, "r") as part_fasta_f:
                #full_fasta.write(part_fasta_f.read())
                sequences=SeqIO.parse(part_fasta_f, "fasta")
                for seq in sequences:
                    #print dir(seq)
                    if seq.id not in in_IDs:
                    #if seq not in SeqRec_out_list:
                        #print dir(seq)
                        #print seq.id
                        in_IDs.append(seq.id)
                        SeqRec_out_list.append(seq)

    # Writing the fasta file
    with open(full_unidentified_blast_fasta_f, "a+") as full_fasta:
        SeqIO.write(SeqRec_out_list, full_fasta, "fasta")
                    
    #print '\tFound and combined', i, 'island unidentified CDS FASTA files'
    return i

def produce_tree_from_fasta(faa_f, tree_f):
    ''' This function creates a tree file from a fasta file
    Input
    
    @faa_f - a fasta containing a sequences in amino notation

    @tree_f - a tree file name that will be created in newick format
    
    '''

    from Bio.Align.Applications import ClustalwCommandline

    print '\t Constructing multiple alignment based on fasta file'
    clustalw_exe = os.path.join(os.getcwd(), "clustalw2\\clustalw2.exe")
    #print clustalw_exe
    #print faa_f
    cmd_line = ClustalwCommandline(clustalw_exe, infile=faa_f)
    cmd_line()
    print '\t Constracting multiple alignment based on fasta file has finished'
    
    print '\t Constracting distance matrix based on multiple alignment'
    from Bio.Phylo.TreeConstruction import DistanceCalculator
    from Bio.Phylo.TreeConstruction import DistanceTreeConstructor
    from Bio import AlignIO

    align_f = os.path.splitext(faa_f)[0]+'.aln'
    align = AlignIO.read(align_f, "clustal")
    
    calculator = DistanceCalculator('identity')
    distance_matrix = calculator.get_distance(align)
    #constructor = DistanceTreeConstructor(calculator, 'nj')
    #tree = constructor.build_tree(aln)
    print '\t Constracting distance matrix based on multiple alignment has finished'


    print '\t Constracting distance matrix based tree'
    from Bio import Phylo
    constructor = DistanceTreeConstructor()
    tree = constructor.nj(distance_matrix)
    Phylo.write(tree, tree_f, 'newick')
    print '\t Constracting distance matrix based tree has finished'
    
def write_log_line(log_f, log_line):
    ''' This function writes a line to the log file
    Input
    
    @log_f - log file name

    @log_line - log line to write
    
    '''

    with open(log_f, 'a') as log_h:
        log_h.write(log_line+'\n')

def read_gff_to_sorted_dict(gff_file_name):
    ''' This function reads the gff file that maps CDS to dictionary form
    This is used in various stages for GFF reading.

    Input
    
    @gff_file_name - a GFF file to read

    Output

    @returns - a dictionary with metasequence manes as keys and lists of
    CDS sorted by starting place as values
    '''
    # !!!
    # Might need to change CDS ID to locus tag
    # !!!
    
    # Start with reading the tuples              
    with open(gff_file_name, 'r') as gff_file:
        big_list = []
        for line in gff_file.readlines():
            #print line
            #arguments = re.findall('^([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\tID=([^;]*)' ,line)[0]
            arguments = re.findall('^([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\tID=([^;]*);locus_tag=([^;]*)' ,line)

            # If no right line found
            if arguments == []:
                continue
            else:
                # Taking extracted tuple
                arguments = arguments[0]

            #if len(arguments) == 9:
            if len(arguments) == 10:
                # Like GBSCSSed77CDRAFT_c000001
                big_sequence = arguments[0]
                # Like GMP
                feature_source = arguments[1]
                # Like CDS
                feature_type = arguments[2]
                # Like 6995
                feature_start = arguments[3]
                # Like 8098
                feature_end = arguments[4]
                # Like . or null
                feature_score = arguments[5]
                # Like 1 or -1
                feature_strand = arguments[6]
                # Like 0, 1 or 2
                feature_frame = arguments[7]
                # Like GBSCSSed77CDRAFT_00000110.1
                feature_id = arguments[8]
                # Like GBSCSSed77CDRAFT_000001101
                feature_tag_locus = arguments[9]

                #Lets add this record to the list if it is a CDS record
                if feature_type == 'CDS' or feature_type.lower() == 'CDS'.lower():
                    #big_list.append([big_sequence, feature_id, int(feature_start), int(feature_end), feature_strand])
                    #print [big_sequence, feature_tag_locus, int(feature_start), int(feature_end), feature_strand]
                    big_list.append([big_sequence, feature_tag_locus, int(feature_start), int(feature_end), feature_strand])

        #Sort the list by 1 - meta sequence, 2 - strand, 3 - feature start
        #big_list.sort(key = lambda x:(x[0], x[4], x[2]))

        #Sort the list by 1 - meta sequence, 2 - feature start
        big_list.sort(key = lambda x:(x[0], x[2]))

        #Produce dictionary instead of list
        big_dict = {}
        last_meta = []
        for feature in big_list:
            if feature[0] != last_meta:
                last_meta = feature[0]
                big_dict[last_meta] = []
            big_dict[last_meta].append(feature[1:])
            
    return big_dict



def filter_fasta_file(in_fasta, out_fasta, included_cds):
    ''' This function takes the input FASTA file and writes to output FASTA
    only specific CDS from the provided list

    Input
    
    @in_fasta - input FASTA file

    @out_fasta - Output FASTA file

    @included_cds - a list of CDS (fasta tags) to include in output FASTA file
    '''

    SeqRec_out_list = []
    
    full_fasta_dict = SeqIO.index(in_fasta, "fasta")
    for cds in included_cds:
        try:
            SeqRec_out_list.append(full_fasta_dict[cds]) 
        except:
            print '\t',cds, 'was not found in the fasta file'
    with open(out_fasta, "w") as out_fasta_f:
        SeqIO.write(SeqRec_out_list, out_fasta_f, "fasta")

    print '\tFiltering produced', len(SeqRec_out_list), 'CDS seqs out of initial', len(full_fasta_dict), 'CDS seqs'

    return len(SeqRec_out_list)

    
def filter_short_src_faa(faa_f, out_faa_f, gff_f, th):
    ''' This function takes the input FASTA file and writes to output FASTA
    only CDS which belong to meta-sequence sources longer (CDS number) than threshold

    Input
    
    @faa_f - input FASTA file

    @out_faa_f - Output FASTA file

    @gff_f - a source meta-sequence GFF file to check the belonging of CDS
    to shorter (CDS number) source sequences

    @th - minimum CDS number in meta-sequence treshold. Meta-sequence shorter
    than this will be considered small and it's CDS will not be included
    in the final FASTA
    '''

    # Read GFF source statistics
    stat = examine_gff(gff_f)

    long_src = [k for k, v in stat.iteritems() if (v >= int(th))]
    
    # Prepare CDS for filtered output
    cds_for_filt_out = []
    with open(gff_f) as gff_hnd:
        for rec in GFF.parse(gff_hnd, limit_info=dict(gff_id = long_src)):
            for f in rec.features:
                if f.type == 'CDS' or f.type.lower() == 'cds':
                    cds_for_filt_out.append(f.qualifiers['locus_tag'][0])
                    #cds_for_filt_out.append(f.id)

    # Filtering FASTA
    out_num = filter_fasta_file(faa_f, out_faa_f, cds_for_filt_out)
    
    return out_num


