# Project:  pyhmmer/islander
# Date:     25/06/2016
# Author:   Anton Shifman [santonsh[at]gmail.com]
# Supervisor: Noam Grinmberg
# File version: 1.0
#
# Description: This is a script to auto configure the metagenomic
# databases present in Input/Sequences directory for island identification runs
# The script accepts hmm profiles file, hmm profile weights file, thresholds and create config.json
# files for each metagenome directory to be used by island identificator later
# If one of this parameters is not given the script will take the value from
# the def_config.json file. The functionality of this script is reflected
# in pyhmmer_gui.py GUI script so if you are using this GUI you can skip
# this script


import os, sys, json, getopt

from pyhmmer_io_aux import read_hmm_weights
from pyhmmer_io_aux import get_metaseq_fasta_size
from pyhmmer_io_aux import examine_gff
from pyhmmer_io_aux import hmm_profiles_number
from pyhmmer_io_aux import auto_detect_file

def autoconfig_all(argv):

    # Loading default config settings
    with open('def_config.json') as config_file:    
        config = json.load(config_file)
    #print argv
    # Parsing the arguments
    try:
        opts, args = getopt.getopt(argv,"h",["help", "hmm_file=","hmmw_file=",
                                             "eval=", "len=", "avgw="])
        for opt, arg in opts:
            if opt in ("-h", "--help"):
                print 'Usage: autoconfigurator.py --hmm_file <rel_path_to_hmm_profiles> --hmmw_file <rel_path_to_hmm_profiles_weights> --eval <min isle eval threshold> --len <min isle len threshold> --avgw <min avg weight isle threshold>'
                return
            elif opt in ("--hmm_file"):
                config['hmm_file'] = arg
            elif opt in ("--hmmw_file"):
                config['hmm_wght_file'] = arg
            elif opt in ("--eval"):
                config['min_eval_th'] = arg
            elif opt in ("--len"):
                config['min_len_th'] = arg
            elif opt in ("--avgw"):
                config['min_avg_wght_th'] = arg
            else:
                pass
    #sys.exit()
    except:
        print '\tE-Something went wrong with the arguments. Please check your arguments'
        print 'Usage: autoconfigurator.py --hmm_file <rel_path_to_hmm_profiles> --hmmw_file <rel_path_to_hmm_profiles_weights> --eval <min isle eval threshold> --len <min isle len threshold> --avgw <min avg weight isle threshold>'
        pass

    # Checking global config structure
    # Checking thresholds
    print '\nChecking thresholds'
    val = config['min_len_th']
    try:
        val = int(val)
    except:
        print '\tE-Wrong value choosen for minimal isle length'
        return
    if val>0:
        pass
    else:
        print '\tE-Wrong value choosen for minimal isle length'
        return

    try:
        config['min_avg_wght_th'] = float(config['min_avg_wght_th'])
    except:
        print '\tE-Wrong value choosen for minimal average isle weight'
        return
    if config['min_avg_wght_th']>=0:
        pass
    else:
        print '\tE-Wrong value choosen for minimal average isle weight'
        return

    val = config['min_eval_th']
    try:
        val = float(val)
    except:
        print '\tE-Wrong value choosen for minimal isle e-value'
        return
    if val>=0:
        pass
    else:
        print '\tE-Wrong value choosen for minimal isle e-value'
        return
    
    print '\tI-Done'
            
    # Checking hmm profiles
    print '\nChecking hmm profiles and hmm profile weights file'
    try:
        open(os.path.join(os.getcwd(), config['hmm_file']), 'r')
    except:
        print '\tE-Couldnt open hmm profiles file:', os.path.join(os.getcwd(), config['hmm_file'])
        return
    records_number = hmm_profiles_number(os.path.join(os.getcwd(), config['hmm_file']))
    if records_number<1:
        print '\tE-No hmm profiles found in the given hmm file. Please try again'
        print '\tE-Bad hmm profiles file is:', os.path.join(os.getcwd(), config['hmm_file']) 
        return
    else:
        print '\tI-Cool. Found ' + str(records_number) + ' HMM profiles'
        config['hmm_file'] = os.path.relpath(config['hmm_file'])

    # Checking hmm profile weights file
    if config['hmm_wght_file'] != '':
        try:
            open(os.path.join(os.getcwd(), config['hmm_wght_file']), 'r')
        except:
            print '\tE-Couldnt open hmm profile weights file:', os.path.join(os.getcwd(), config['hmm_wght_file'])
            return
        weights = read_hmm_weights(os.path.join(os.getcwd(), config['hmm_wght_file']))
        if len(weights)<1:
            print '\tE-No hmm profile weights found in the file. Please try again'
            print '\tE-Bad hmm profile weights file is:', os.path.join(os.getcwd(), config['hmm_wght_file'])
            return
        else:
            print '\tI-Cool. Found ' + str(len(weights)) + ' HMM profile wieghts in the file. The HMM profiles not mentioned in this file will get a default weight of 1'
            config['hmm_wght_file'] = os.path.relpath(config['hmm_wght_file'])
                            
    # Checking metagenome folders
    print '\nChecking metagenome folders'
    meta_dir = os.path.join(os.getcwd(), 'Input\Sequences')

    found_metagenomes = []

    files = os.listdir(meta_dir)
    for file in files:
        full_path = os.path.join(os.getcwd(), 'Input\Sequences', file)
        if os.path.isdir(full_path):
            found_metagenomes.append(file)

    print '\tI-Found', str(len(found_metagenomes)), 'metagenome folders in Input\Sequences directory' 

    # Starting each metagenome configuration
    configured_metagenomes = []
    excluded_metagenomes = []
    for meta in found_metagenomes:
        print '\nWorking on', meta, 'directory'
        meta_dir = os.path.join(os.getcwd(), 'Input\Sequences', meta)

        # Find and check faa file
        faa_file = auto_detect_file(meta_dir, '.faa')
        if faa_file == '':
            print '\tE-Skipping this directory due to missing FASTA file'
            continue
        records_number = get_metaseq_fasta_size(faa_file)
        if records_number<1:
            print '\tE-No sequences found in the FASTA. Skipping this directory'
            continue
        print '\tI-Cool. Found ' + str(records_number) + ' CDAs in this metagenome'
                
        # Find and check gff file
        gff_file = auto_detect_file(meta_dir, '.gff')
        if gff_file == '':
            print '\tE-Skipping this directory due to a missing GFF file'
            continue
        gff_stats = examine_gff(gff_file)
        records_number = len(gff_stats)
        if records_number<1:
            print '\tE-No source sequences found in the GFF file. Skipping this directory'
            continue
        print '\tI-Cool. Found ' + str(records_number) + ' source sequences in this metagenome'

        # complete config structure with faa and gff
        config['faa_file'] = os.path.relpath(faa_file)
        config['gff_file'] = os.path.relpath(gff_file)

        # write the config
        with open(os.path.join(meta_dir, 'config.json') , 'w') as config_file:    
            json.dump(config, config_file, indent=4, sort_keys=True)

        configured_metagenomes.append(meta)

    # Reporting    
    excluded_metagenomes = [x for x in found_metagenomes if x not in configured_metagenomes]
    print '\nConfiguration finished'
    print 'Configured metagenomes are:'
    for meta in configured_metagenomes:
        print '\t', meta
    print 'Skipped metagenomes are:'
    for meta in excluded_metagenomes:
        print '\t', meta


if __name__ == "__main__":
   autoconfig_all(sys.argv[1:])
