# Project:  pyhmmer/islander
# Date:     25/06/2016
# Author:   Anton Shifman [santonsh[at]gmail.com]
# Supervisor: Noam Grinmberg
# File version: 1.0
#
# Description: This file main top level batch scripts of pyhhmer/islander
# The run_stages_for_all_metagenomes function can be used to run all the stages
# one by one and run a common stage of analyzing unidentified CDS metagenome
# frequency 



from pyhmmer_io_aux import read_gff_to_sorted_dict
from pyhmmer_io_aux import read_filtered_mapping
from pyhmmer_io_aux import annotate_isles_to_gff
from pyhmmer_io_aux import annotate_isles_to_tbl
from pyhmmer_io_aux import annotate_isles_incl_src_to_gff
from pyhmmer_io_aux import read_isle_tbl_file
from pyhmmer_io_aux import filter_pfamscan_out
from pyhmmer_io_aux import read_pfam_filtered_mapping
from pyhmmer_io_aux import filter_hmmerscan_domtlb_out
from pyhmmer_io_aux import filter_short_src_faa
from pyhmmer_io_aux import read_isle_gff_file
from pyhmmer_io_aux import filter_fasta_file
from pyhmmer_io_aux import open_run_config
from pyhmmer_io_aux import read_hmm_weights
from pyhmmer_io_aux import extract_cds_from_isl_gff
from pyhmmer_io_aux import annotate_unidentified_cds_to_tbl_file
from pyhmmer_io_aux import analyze_island_blast_hits_frequency
from pyhmmer_io_aux import map_cds_features_to_profiles
from pyhmmer_io_aux import write_log_line
from pyhmmer_io_aux import produce_tree_from_fasta
from pyhmmer_io_aux import create_combined_fasta_with_island_related_unidentified_CDS

from pyhmmer_math_aux import find_islands

import time

import subprocess, sys
import os

def run_stages_for_all_metagenomes(stages,
                                   gen_config_f,
                                   gen_input_d,
                                   gen_output_d,
                                   gen_hmmscan_fasta_f,
                                   gen_hmm_out_f,
                                   gen_hmm_filt_out_f,
                                   gen_isl_tbl_f,
                                   gen_isl_gff_f,
                                   gen_isl_with_src_gff_f,
                                   gen_isl_pfam_gff_f,
                                   gen_isl_pfam_with_src_gff_f,
                                   gen_pfam_fasta_f,
                                   gen_pfam_out_f,
                                   gen_pfam_filt_out_f,
                                   gen_blast_fasta_f,
                                   gen_common_d,
                                   gen_combined_metagenome_fasta_f,
                                   gen_combined_nonidentified_fasta_f,
                                   gen_blast_db_f,
                                   gen_blast_out_f,
                                   gen_in_island_CDS_lisf_f,
                                   gen_unid_out_f,
                                   rerun_flag = True):
    ''' This will run all selected stages (in stages dict) for all metagenomes and then the common blast stage

    a. run_hmmer_stage
    b. identify_islands_stage
    c. run_pfam_unidentifed_stage
    d. run_unidentified_fasta_annotation_stage
    e. run_blst

    Input to the function
    @stages - dictionary with boolean values indicating which stages to run

    @gen_input_d - input directory to look for metagenomes

    @gen_output_d - output ditory

    @gen_config_f - general config file to look for in the directory
    
    @gen_hmmscan_fasta_f - file for hmmscan to work on (may be smaller than original FASTA file due to short
        metasequences filtering for performance optimization)

    @gen_hmm_out_f - HMMER scan output file

    @gen_hmm_filt_out_f - HMMER scan output file filtered of big expectation
        value matches and duplicate matches

    @gen_isl_gff_f - output file for found islands annotation in GFF format 

    @gen_isl_tbl_f - output file for found islands annotation in table format
        (for scripts useage)

    @gen_isl_pfam_gff_f - output file for found islands annotation in GFF format
        after previously unidentified CDS were found as PFAM profiles
   
    @gen_pfam_fasta_f - smaller FASTA file for PFAM stage containing only previously
        unidentified CDS within detected islands  

    @gen_pfam_out_f - PFAM scan output file

    @gen_pfam_filt_out_f - PFAM scan output file filtered of big expectation
        value matches and duplicate matches
    
    @gen_blast_fasta_f - smaller FASTA file for BLAST stage containing only
        unidentified CDS (after HMMER and PFAM) within detected islands
        for further blast search in whole metagenome data to reveal the CDS frequency

    @rerun_flag - boolean flag indication weather the underlying scripts should be run again even if output files exist

    '''

    # Checking input metagenomes
    files = os.listdir(gen_input_d)
    metas = []
    for f in files:
        config_f = os.path.join(os.getcwd(), gen_input_d, f, gen_config_f)
        #print part_faa_f
        if os.path.exists(config_f):
            metas.append(f)
            print '\tFound configured meta - ', f, '!'

    print '\tFound ', len(metas), 'ready to run metagenomes'

    # Log file name
    log_f = os.path.join(os.getcwd(), gen_output_d, gen_common_d, time.strftime('all_%d%m%Y_%H%M')+'.log')
    
    # running the meta directories
    for meta in metas:

        # Preparing input
        config_f = os.path.join(os.getcwd(), gen_input_d, meta, gen_config_f)
        config = open_run_config(config_f)
        faa_f = config['faa_file']
        meta_gff_f = config['gff_file']
        hmm_prfl_f = config['hmm_file']
        hmm_wghts_f = config['hmm_wght_file']
        len_th = config['min_len_th']
        e_th = config['min_eval_th']
        wght_th = config['min_avg_wght_th']

        # Preparing output
        if not os.path.exists(os.path.join(os.getcwd(), gen_output_d, meta)):
            os.makedirs(os.path.join(os.getcwd(), gen_output_d, meta))
    
        hmmscan_fasta_f = os.path.join(os.getcwd(), gen_output_d, meta, gen_hmmscan_fasta_f)
        hmm_out_f = os.path.join(os.getcwd(), gen_output_d, meta, gen_hmm_out_f)
        hmm_filt_out_f = os.path.join(os.getcwd(), gen_output_d, meta, gen_hmm_filt_out_f)
        isl_gff_f = os.path.join(os.getcwd(), gen_output_d, meta, gen_isl_gff_f)
        isl_with_src_gff_f = os.path.join(os.getcwd(), gen_output_d, meta, gen_isl_with_src_gff_f)
        isl_tbl_f = os.path.join(os.getcwd(), gen_output_d, meta, gen_isl_tbl_f)
        isl_pfam_gff_f = os.path.join(os.getcwd(), gen_output_d, meta, gen_isl_pfam_gff_f)
        isl_pfam_with_src_gff_f = os.path.join(os.getcwd(), gen_output_d, meta, gen_isl_pfam_with_src_gff_f)
        pfam_fasta_f = os.path.join(os.getcwd(), gen_output_d, meta, gen_pfam_fasta_f)
        pfam_out_f = os.path.join(os.getcwd(), gen_output_d, meta, gen_pfam_out_f)
        pfam_filt_out_f = os.path.join(os.getcwd(), gen_output_d, meta, gen_pfam_filt_out_f)     
        blast_fasta_f = os.path.join(os.getcwd(), gen_output_d, meta, gen_blast_fasta_f)

        # running the stages
        print '\n#######################################################'
        print '######      Running ', meta, ' directory'
        print '#######################################################'
        
        out = run_all_preblast_stages_for_single_metagenome(stages,
                                                      faa_f,
                                                      meta_gff_f,
                                                      hmmscan_fasta_f,
                                                      hmm_prfl_f,
                                                      hmm_wghts_f,
                                                      hmm_out_f,
                                                      hmm_filt_out_f,
                                                      isl_gff_f,
                                                      isl_with_src_gff_f,
                                                      isl_tbl_f,
                                                      isl_pfam_gff_f,
                                                      isl_pfam_with_src_gff_f,
                                                      len_th,
                                                      e_th,
                                                      wght_th,
                                                      pfam_fasta_f,
                                                      pfam_out_f,
                                                      pfam_filt_out_f,
                                                      blast_fasta_f,
                                                      rerun_flag)
        # Write log line
        out = meta + '\t' + out
        write_log_line(log_f, out)

    # Running the common stage
    if stages['blast']:
        full_blast_fasta_f              = os.path.join(os.getcwd(), gen_output_d, gen_common_d, gen_combined_metagenome_fasta_f)
        full_unidentified_blast_fasta_f = os.path.join(os.getcwd(), gen_output_d, gen_common_d, gen_combined_nonidentified_fasta_f)
        unidentified_CDS_tree_f         = os.path.join(os.getcwd(), gen_output_d, gen_common_d, 'unidenfified_CDS_tree.nwk')
        full_unid_out_f                 = os.path.join(os.getcwd(), gen_output_d, gen_common_d, gen_unid_out_f)
        blast_db_f                      = os.path.join(os.getcwd(), gen_output_d, gen_common_d, gen_blast_db_f)
        blast_out_f                     = os.path.join(os.getcwd(), gen_output_d, gen_common_d, gen_blast_out_f)
        in_island_CDS_lisf_f            = os.path.join(os.getcwd(), gen_output_d, gen_common_d, gen_in_island_CDS_lisf_f)
        if not os.path.exists(os.path.join(os.getcwd(), gen_output_d, gen_common_d)):
                os.makedirs(os.path.join(os.getcwd(), gen_output_d, gen_common_d))

        print '\n#######################################################'
        print '#######################################################'
        print '######      Running the common stage'
        print '#######################################################'
        print '#######################################################'
        
        run_blast_search_unidentified_stage(gen_input_d,
                                            gen_output_d,
                                            gen_isl_gff_f,
                                            gen_isl_pfam_gff_f,
                                            gen_blast_fasta_f,
                                            gen_config_f,
                                            full_unidentified_blast_fasta_f,
                                            full_blast_fasta_f,
                                            blast_db_f,
                                            blast_out_f,
                                            in_island_CDS_lisf_f,
                                            gen_unid_out_f,
                                            full_unid_out_f,
                                            unidentified_CDS_tree_f,
                                            rerun_flag)



def run_all_preblast_stages_for_single_metagenome(stages,
                                                  faa_f,
                                                  meta_gff_f,
                                                  hmmscan_fasta_f,
                                                  hmm_prfl_f,
                                                  hmm_wghts_f,
                                                  hmm_out_f,
                                                  hmm_filt_out_f,
                                                  isl_gff_f,
                                                  isl_with_src_gff_f,
                                                  isl_tbl_f,
                                                  isl_pfam_gff_f,
                                                  isl_pfam_with_src_gff_f,
                                                  len_th,
                                                  e_th,
                                                  wgh_th,
                                                  pfam_fasta_f,
                                                  pfam_out_f,
                                                  pfam_filt_out_f,
                                                  blast_fasta_f,
                                                  rerun_flag = True):
    ''' This will run all pre BLAST stages including:

    a. run_hmmer_stage
    b. identify_islands_stage
    c. run_pfam_unidentifed_stage
    d. run_unidentified_fasta_annotation_stage

    Input to the function
    @stages - dictionary with boolean values indicating which stages to run
    
    @faa_f - metagenome FASTA file

    @meta_gff_f - metagenome GFF file containing metagenome sources with
        corresponding fasta CDS sequences
    
    @hmmscan_fasta_f - file for hmmscan to work on (may be smaller than original FASTA file due to short
        metasequences filtering for performance optimization)

    @hmm_prfl_f - HMM profiles file containing profiles for the scan

    @hmm_out_f - HMMER scan output file

    @hmm_filt_out_f - HMMER scan output file filtered of big expectation
        value matches and duplicate matches

    @isl_gff_f - output file for found islands annotation in GFF format 

    @isl_tbl_f - output file for found islands annotation in table format
        (for scripts useage)

    @isl_pfam_gff_f - output file for found islands annotation in GFF format
        after previously unidentified CDS were found as PFAM profiles

    @len_th - island length threshold used in island identification stage
        and pre-hmmerscan stage

    @e_th - island e-value threshold used in island identification stage

    @wgh_th - island average wieght threshold used in island identification stage    
    
    @pfam_fasta_f - smaller FASTA file for PFAM stage containing only previously
        unidentified CDS within detected islands  

    @pfam_out_f - PFAM scan output file

    @pfam_filt_out_f - PFAM scan output file filtered of big expectation
        value matches and duplicate matches
    
    @blast_fasta_f - smaller FASTA file for BLAST stage containing only
        unidentified CDS (after HMMER and PFAM) within detected islands
        for further blast search in whole metagenome data to reveal the CDS frequency

    @rerun_flag - boolean flag indication weather the underlying scripts should be run again even if output files exist

    '''

    hmmer_out =     'SKIPPED'
    isl_out =       'SKIPPED'
    pfam_out =      'SKIPPED'
    preblast_out =  'SKIPPED'
    
    if stages['hmmer']:
        hmmer_out = run_hmmer_stage(faa_f, hmmscan_fasta_f, meta_gff_f, len_th, hmm_prfl_f, hmm_out_f, hmm_filt_out_f, rerun_flag)
        
    if stages['islands'] and hmmer_out != 'FAILED' and hmmer_out != 'NOT RUN':
        isl_out = identify_islands_stage(hmm_filt_out_f, meta_gff_f, hmm_wghts_f, isl_tbl_f, isl_gff_f, isl_with_src_gff_f, len_th, e_th, wgh_th, rerun_flag)

    if stages['pfam'] and isl_out != 'FAILED' and isl_out != 'NO ISLANDS FOUND':
        pfam_out = run_pfam_unidentifed_stage(faa_f, meta_gff_f, hmm_wghts_f, pfam_fasta_f, isl_tbl_f, isl_pfam_gff_f, isl_pfam_with_src_gff_f, pfam_out_f, pfam_filt_out_f, hmm_filt_out_f, rerun_flag)
        
    if stages['pre_blast'] and pfam_out != 'FAILED':
        preblast_out = run_unidentified_fasta_annotation_stage(faa_f, blast_fasta_f, isl_pfam_gff_f, rerun_flag)


    log_txt = ('HMMER:'+hmmer_out+',').ljust(22) + ('ISLANDS:'+isl_out+',').ljust(27) + ('PFAM:'+pfam_out+',').ljust(22) + ('PRE_BLAST:'+preblast_out+',').ljust(22)
    return log_txt



def run_blast_search_unidentified_stage(input_d, output_d, gen_isl_gff_f, gen_isl_pfam_gff_f, partial_blast_fasta_f_name, config_f_name, full_unidentified_blast_fasta_f, full_blast_fasta_f, blast_db_f, blast_out_f, in_island_CDS_lisf_f, gen_unid_out_f, full_unid_out_f, unidentified_CDS_tree_f, rerun_flag = True):
    ''' This implements stage that searches for interesting CDS in whole metagenome (all input folders) using BLAST  

    To implement this stage the next sub tasks have to be done
    a. Produce and combine unidentifies CDS FASTA files to one full FASTA file for BLAST search
    b. Produce and combine source meta FASTA files to one full FASTA file for BLAST search
    c. Run BLAST of FASTA from b against FASTA from a
    d. Filter the BLAST output
    e. Annotate unidentified CDS frequency

    Input to the function
    @output_d - output directory containing the outputs of different metagenomes
    @partial_blast_fasta_f_name - generic name of FASTA file of unidentified CDS in metagenome output
    @full_fasta_f_name - generic name of full metagenome FASTA file
    @full_unidentified_blast_fasta_f - file to save full FASTA file of all unidentified CDS
    @full_blast_fasta_f - a complete FASTA file containing all the metagenome faa files
    @rerun_flag - boolean flag indication weather the script should be run again even if output files exist

    '''

    # If no need to rerun then return
    if rerun_flag == False and os.path.exists(full_unid_out_f) and os.path.exists(in_island_CDS_lisf_f) and os.path.exists(blast_out_f) and os.path.exists(blast_db_f) and os.path.exists(full_unidentified_blast_fasta_f) and os.path.exists(full_blast_fasta_f) and os.path.exists(unidentified_CDS_tree_f):
        print '\t-I: Rerun flag is set to False and all the output files exist thus skipping HMMER scan stage'
        return 'OUTPUT EXISTS'

    # Remove the previous run files
    try:
        os.remove(full_unidentified_blast_fasta_f)
    except OSError:
        pass
    try:
        os.remove(full_blast_fasta_f)
    except OSError:
        pass
    try:
        os.remove(blast_db_f)
    except OSError:
        pass
    try:
        os.remove(blast_out_f)
    except OSError:
        pass
    try:
        os.remove(in_island_CDS_lisf_f)
    except OSError:
        pass
    try:
        os.remove(full_unid_out_f)
    except OSError:
        pass
    try:
        os.remove(unidentified_CDS_tree_f)
    except OSError:
        pass
    
    print '\nStarting island unidentified CDS frequency evaluation stage'


    # Creating full in-island CDS list for further use
    print '\n\tCreating full in-island CDS list for further use...'
    start = time.time()

    with open(in_island_CDS_lisf_f, "w") as cds_list_file:
        files = os.listdir(output_d)
        i = 0
        j = 0
        for dir in files:
            isl_f = os.path.join(os.getcwd(), output_d, dir, gen_isl_gff_f)
            if os.path.exists(isl_f):
                i = i + 1
                cds_list = extract_cds_from_isl_gff(isl_f)
                j = j + len(cds_list)
                for cds in cds_list:
                    cds_list_file.write(cds+'\n')
                    
        print '\tFound and analyzed', i, 'island GFF files'
        print '\tFound', j, 'in-island CDS'
        
    print '\tCreating full in-island CDS list for further use has finished in', str(int(time.time()-start)), 'seconds'



    # Creating common FASTA file
    print '\n\tCreating common FASTA files...'
    print '\tCreating common FASTA file with unidentified CDS found within the islands...'
    start = time.time()

    i = create_combined_fasta_with_island_related_unidentified_CDS(full_unidentified_blast_fasta_f, output_d, partial_blast_fasta_f_name)

    print '\tFound and combined', i, 'island unidentified island related CDS FASTA files'



    # Creating full FASTA file consisting all input metagenome FASTA files
    print '\tCreating common FASTA file of all CDS found in all metasequences...'

    from sets import Set
    in_IDs = Set()
    import re
    files = os.listdir(input_d)
    i = 0
    copy_flag = True
    with open(full_blast_fasta_f, "w") as full_fasta:
        for dir in files:
            config_f = os.path.join(os.getcwd(), input_d, dir, config_f_name)
            if os.path.exists(config_f):
                # Loading config
                config = open_run_config(config_f)
                meta_fasta_f = config['faa_file']
                if os.path.exists(meta_fasta_f):
                    i = i + 1
                    #print 'i:', i, ' in_IDs len:', len(in_IDs)
                    with open(meta_fasta_f, "r") as meta_fasta_h:
                        lines = meta_fasta_h.readlines()
                        #start = time.time()
                        # Go throught lines of fasta files
                        for line in lines:
                            if line[0] == '>':
                                id = re.findall('>\s*(.*)\s*.*', line)[0].lower()
                                # Check if this id is already copied
                                if id not in in_IDs:
                                    in_IDs.add(id)
                                    copy_flag = True
                                else:
                                    copy_flag = False
                            # Copy the line to the big fasta file if the ID is new
                            if copy_flag == True:
                                full_fasta.write(line)
                                
    print '\tFound and combined', i, 'source metasequence FASTA files'
    print '\tCreating common FASTA files has finished in', str(int(time.time()-start)), 'seconds'



    # Constracting BLAST db      
    print '\n\tConstructing BLAST db for BLAST run'
    start = time.time()
    makeblastdb_cmnd = "blast-2.3.0\\bin\\makeblastdb.exe" + " -in " + full_blast_fasta_f + ' -parse_seqids -dbtype prot ' + ' -out ' + blast_db_f + ' -max_file_sz 2000000000'
    try:
        s = subprocess.check_output(makeblastdb_cmnd, stderr=subprocess.STDOUT)
        print "\tmakeblastdb has finished"
    except subprocess.CalledProcessError as e:
        print "\t-E: Looks like the makeblastdb has failed. Please check the hmm profile inputs"
        print e.output
        sys.exit()

    print '\tConstructing BLAST db for BLAST run has finished in', str(int(time.time()-start)), 'seconds'



    # Running BLAST
    print '\n\tRunning BLAST for unidentified CDS against common database'
    start = time.time()
    blast_eval = 0.001
    blastp_cmnd =      "blast-2.3.0\\bin\\blastp.exe"      ' -query ' + full_unidentified_blast_fasta_f + ' -db ' + blast_db_f + ' -out ' + blast_out_f + ' -evalue ' + str(blast_eval) + ' -outfmt 5 '
    #print blastp_cmnd
    try:
        s = subprocess.check_output(blastp_cmnd, stderr=subprocess.STDOUT)
        print "\tblastp has finished"
    except subprocess.CalledProcessError as e:
        print "\t-E: Looks like the blastp has failed. Please check the inputs"
        print e.output
        sys.exit()

    print '\tRunning BLAST for unidentified CDS against common database has finished in', str(int(time.time()-start)), 'seconds'



    # Counting BLAST hits inside and outside of islands
    print '\n\tCounting BLAST hits inside and outside of islands...'
    start = time.time()

    query_in_island_frequency = analyze_island_blast_hits_frequency(blast_out_f, in_island_CDS_lisf_f)
        
    print 'len', len(query_in_island_frequency.keys())
    print '\tCounting BLAST hits inside and outside of islands has finished in', str(int(time.time()-start)), 'seconds'
    


        
    # Prepare unidentified CDS output file
    print '\n\tPrepare unidentified CDS output file...'
    start = time.time()

    with open(full_unid_out_f, "a+") as full_unid_out:

        dirs = os.listdir(output_d)
        i = 0
        for dir in dirs:
            isl_pfam_gff_f  = os.path.join(os.getcwd(), output_d, dir, gen_isl_pfam_gff_f)
            unid_out_f 	    = os.path.join(os.getcwd(), output_d, dir, gen_unid_out_f)
            if os.path.exists(isl_pfam_gff_f):
                i = i + 1
                # Prepare unidentified CDS output file
                annotate_unidentified_cds_to_tbl_file(isl_pfam_gff_f, dir, unid_out_f, query_in_island_frequency)
                with open(unid_out_f, "r") as part_f:
                    full_unid_out.write(part_f.read())
                    
    print '\tProduced', i, 'unidentified CDS output files'
        
    print '\tPrepare unidentified CDS output file has finished in', str(int(time.time()-start)), 'seconds'



    print '\n\tProducing unidentified CDS alignment and a tree file'
    start = time.time()    
    produce_tree_from_fasta(full_unidentified_blast_fasta_f, unidentified_CDS_tree_f)
    print '\n\tProducing unidentified CDS alignment and a tree file has finished in', str(int(time.time()-start)), 'seconds'



    print '\nIsland unidentified CDS frequency evaluation stage has finished'

    return 'DONE'
    
def run_unidentified_fasta_annotation_stage(faa_f, blast_fasta_f, isl_gff_f, rerun_flag = True):
    ''' This implements stage that saves all the unidentified CDS which belongs to islands to a FASTA file that will be used later to check the CDS redundency in metagenome database  

    To implement this stage the next sub tasks have to be done
    a. Read the island GFF file
    b. Filter the input FASTA file and write output FASTA file
    

    Input to the function
    @faa_f - metagenome FASTA file
    @blast_fasta_f - output FASTA file produced by filtering faa_f to include only unidentified island CDS (used for BLAST later)
    @isl_gff_f - Islands GFF file to get unidentified island CDS
    @rerun_flag - boolean flag indication weather the script should be run again even if output files exist

    '''

    # If no need to rerun then return
    if rerun_flag == False and os.path.exists(blast_fasta_f):
        print '\t-I: Rerun flag is set to False and all the output files exist thus skipping HMMER scan stage'
        return 'OUTPUT EXISTS'

    # Remove the previous run files
    try:
        os.remove(blast_fasta_f)
    except OSError:
        pass
    
    print '\nStarting island unidentified CDS FASTA annotation for BLAST stage'
    
    print '\n\tDiscovering island unidentified CDS for BLAST run...'
    start = time.time()

    try:
        # Read islands GFF file
        print '\n\tReading islands GFF file...'
        islands = read_isle_gff_file(isl_gff_f)
        print '\tReading islands GFF file has finished.'

        cds_for_blast = []
        # Collect CDS for BLAST
        for meta_src in islands.keys():
            for isl in islands[meta_src]:
                for cds in isl['CDS']:
                    if cds[2] == 'none':
                        cds_for_blast.append(cds[1])
    except:
        print '\t-E: Discovering island unidentified CDS for BLAST run has failed. Does island GFF file exist?' 
        return 'FAILED'

    print '\tDiscovering island unidentified CDS for BLAST run has finished in', str(int(time.time()-start)), 'seconds'


    # Write FASTA file for BLAST run
    try:
        print '\n\tPreparing fasta file for BLAST run...'
        start = time.time()
        filter_fasta_file(faa_f, blast_fasta_f, cds_for_blast)
        print '\tPreparing fasta file for BLAST run has finished in', str(int(time.time()-start)), 'seconds'
    except:
        print '\t-E: FASTA file for BLAST run preparation has failed'
        return 'FAILED'



    print '\nIsland unidentified CDS FASTA annotation for BLAST stage has finished'

    return 'DONE'



def run_hmmer_stage(faa_f, hmmscan_fasta_f, meta_gff_f, len_th, hmm_prfl_f, hmm_out_f, hmm_filt_out_f, rerun_flag = True):
    ''' This implements HMMER scan run on metasequence FASTA file

    To implement this stage the next sub tasks have to be done
    a. Run hmmpress to prepare HMM profiles for hmmscan run
    b. Run hmmscan
    c. Filter hmmscan output

    Input to the function
    @faa_f - metagenome FASTA file
    @hmmscan_fasta_f - auxilary FASTA file produced by filtering faa_f file from CDS related to short meta sequences (used for HMMER scan)
    @meta_gff_f - metagenome source sequences (used here to determine short meta sequences for input FASTA filtering)
    @len_th - island length threshold (used here to filter faa_f file of CDS related to short meta sequences)
    @hmm_prfl_f - HMM profiles file to use
    @hmm_out_f - HMM scan domtbl output file
    @hmm_filt_out_f - reduced HMM output file (used as CDS-HMM profile mapping)
    @rerun_flag - boolean flag indication weather the script should be run again even if output files exist

    '''

    # If no need to rerun then return
    if rerun_flag == False and os.path.exists(hmm_out_f) and os.path.exists(hmm_filt_out_f):
        print '\t-I: Rerun flag is set to False and all the output files exist thus skipping HMMER scan stage'
        return 'OUTPUT EXISTS'

    # Remove the previous run files
    try:
        os.remove(hmmscan_fasta_f)
    except OSError:
        pass
    try:
        os.remove(hmm_out_f)
    except OSError:
        pass
    try:
        os.remove(hmm_filt_out_f)
    except OSError:
        pass

    print '\nRunning HMMER scan stage'

    # Filter faa_f file from CDS related to short meta sequences to reduce runtime
    if int(len_th) > 1:
        print '\n\tFiltering input FASTA file...'
        start=time.time()
        try:
            out_num = filter_short_src_faa(faa_f, hmmscan_fasta_f, meta_gff_f, len_th)
        except:
            #out_num = filter_short_src_faa(faa_f, hmmscan_fasta_f, meta_gff_f, len_th)
            return 'FAILED'
        print "\tFiltering input FASTA file has finished in", str(int(time.time()-start)), "seconds"
        if out_num<int(len_th):
            print '\t-E: Too few fasta sequences was produced by threshold filtering. Returning'
            return 'NOT RUN'
    else:
        hmmscan_fasta_f = faa_f
    
    # Run HMMER press
    if not os.path.exists(hmm_prfl_f):
        print '\t-E: Input file for hmmpress wasnt found. Skipping hmmpress'
        return 'FAILED'
        
    print '\n\tRunning hmmpress...'
    start=time.time()
    hmmpress_cmnd = "hmmer3.0_windows\hmmpress.exe" + " -f " + hmm_prfl_f
    try:
        s = subprocess.check_output(hmmpress_cmnd, stderr=subprocess.STDOUT)
        print "\thmmpress has finished in", str(int(time.time()-start)), "seconds"
    except subprocess.CalledProcessError as e:
        print "\t-E: Looks like the hmmpress has failed. Please check the hmm profile inputs"
        print e.output
        return 'FAILED'



    # Run HMMER scan
    if not os.path.exists(hmmscan_fasta_f):
        print '\t-E: Input files for hmmscan wasnt found. Skipping hmmscan'
        return 'FAILED'

    print '\n\tRunning hmmscan...'
    start=time.time()
    hmmscan_cmnd = "hmmer3.0_windows\hmmscan.exe " + "-o Output\hmmscan.out --domtblout " + hmm_out_f + " --noali " + hmm_prfl_f +" "+ hmmscan_fasta_f
    try:
        s = subprocess.check_output(hmmscan_cmnd, stderr=subprocess.STDOUT)
        print "\thmmscan has finished in", str(int(time.time()-start)), "seconds"
    except subprocess.CalledProcessError as e:
        print "\t-E: Looks like the hmmpscan has failed. Please check the hmm profile inputs"
        print e.output
        return 'FAILED'



    # Run hmm output filtering
    if not os.path.exists(hmm_out_f):
        print '\t-E: Output file of hmmscan wasnt found. Skipping output filtering stage'
        return 'FAILED'

    print "\n\thmmscan output filtering..."
    start=time.time()
    filter_hmmerscan_domtlb_out(hmm_out_f, hmm_filt_out_f)            
    print "\thmmscan output filtering has finished in", str(int(time.time()-start)), "seconds"

    print '\nHMMER scan run stage has finished'
    
    return 'DONE'
    
def identify_islands_stage(hmm_filt_out_f, meta_gff_f, hmm_wghts_f, isl_tbl_f, isl_gff_f, isl_with_src_gff_f, len_th, e_th, wgh_th, rerun_flag = True):
    ''' This implements island identification stage

    To implement this stage the next sub tasks have to be done
    a. Read metasequence source file
    b. Translate metasequence CDS to HMM profiles
    c. Identification of islands using translated sequences (using  binary representation like 00001010110101110 to identify islands)
    d. Annotating islands to table and GFF files with translation of found CDS to HMM profiles

    Input to the function

    @hmm_filt_out_f - reduced HMM output file (used as CDS-HMM profile mapping)    
    @meta_gff_f - metagenome source sequences (contains islands' CDS)
    @isl_tbl_f - island annotation file in table format (used mainly for island load/save in scripts)
    @isl_gff_f - island annotation file in GFF format containing HMM mappings of CDS (used mainly as user output)
    @len_th - island length threshold
    @e_th - island e-value threshold
    @wgh_th - island average weight threshold
    @rerun_flag - boolean flag indication weather the script should be run again even if output files exist
    
    '''
        
    # If no need to rerun then return
    if rerun_flag == False and os.path.exists(isl_tbl_f) and os.path.exists(isl_gff_f) and os.path.exists(isl_with_src_gff_f):
        print '\t-I: Rerun flag is set to False and all the output files exist thus skipping island identification stage'
        return 'OUTPUT EXISTS'

    # Remove the previous run files
    try:
        os.remove(isl_tbl_f)
    except OSError:
        pass
    try:
        os.remove(isl_gff_f)
    except OSError:
        pass
    try:
        os.remove(isl_with_src_gff_f)
    except OSError:
        pass
    
    print 'Starting the Isle identification step'
    
    # Read hmmer output
    try:
        print '\n\tReading mapping - HMMER filtered output...'
        start = time.time()
        mapping = read_filtered_mapping(hmm_filt_out_f)
        print '\tReading mapping - HMMER filtered output has finished in', str(int(time.time()-start)), 'seconds'
    except:
        print '\t-E: Reading HMMER filtered output had failed.'
        return 'FAILED'
    
    # Read gff sequences
    try:
        print '\n\tReading gff source sequencs...'
        start = time.time()
        src_seq = read_gff_to_sorted_dict(meta_gff_f)
        print '\tReading gff source sequencs has finished in', str(int(time.time()-start)), 'seconds'
    except:
        print '\t-E: Reading GFF had failed.'
        return 'FAILED'
    
    # Reading the weights file
    print '\n\tReading hmm profile weights file...'
    start = time.time()
    weights_dict = {}
    if not os.path.exists(hmm_wghts_f):
        print '\n\tNo HMM profile weight file found. Using default weight for all profiles'
    else:
        weights_dict = read_hmm_weights(hmm_wghts_f)
        print '\n\tFound ', len(weights_dict),'profile weights. For not found profiles default weight will be used'
    #print 'DEBUG', hmm_wghts_f
    weights_dict_keys = weights_dict.keys()
    print '\tReading hmm profile weights file has finished in', str(int(time.time()-start)), 'seconds'

    # Translate the metasequences to hmm profile notation
    print '\n\tTranslating gff source sequencs to defined profiles...'
    start = time.time()
    src_seq_translated = {}
    src_seq_translated_bin = {}
    src_seq_translated_weights = {}
    
    for name, cds in src_seq.iteritems():
        (mapped, binary) = map_cds_features_to_profiles(cds, mapping, 1)
        src_seq_translated[name] = mapped
        src_seq_translated_bin[name] = binary
        weights = []
        for i in range(len(mapped)):
            if mapped[i] in weights_dict_keys:
                weights.append(weights_dict[mapped[i]])
            else:
                weights.append(float(binary[i]))
                
        src_seq_translated_weights[name] = weights
    print '\tTranslating gff source sequencs to defined profiles has finished in', str(int(time.time()-start)), 'seconds'
    
    # Identify the islands
    try:
        print '\n\tIdentifing the islands...'
        start = time.time()
        islands={}
        isl_len = 0
        for meta in src_seq_translated_bin.keys():
            isls = find_islands(src_seq_translated_bin[meta], src_seq_translated_weights[meta], e_th, len_th, wgh_th)
            if len(isls) > 0:
                islands[meta] = isls
                isl_len = isl_len + len(isls)
        print '\tIsland identification has finished in', str(int(time.time()-start)), 'seconds'
        if islands == {}:
            print '\t-W: No islands were identified'
            return 'NO ISLANDS FOUND'
    except:
        print '\t-E: Island identification has failed'
        return 'FAILED'
    
    # Annotate the islands in text file
    try:
        print '\n\tAnnotating the islands to the table and GFF output file...'
        start = time.time()
        annotate_isles_to_tbl(islands, isl_tbl_f)
        #print 'finished tbl'
        annotate_isles_to_gff(islands, src_seq, src_seq_translated, src_seq_translated_weights, isl_gff_f)
        #print 'finished gff'
        annotate_isles_incl_src_to_gff(islands, src_seq, src_seq_translated, src_seq_translated_weights, isl_with_src_gff_f)
        #print 'finished src gff'
        print '\tAnnotating the islands to the table and GFF output file has finished in', str(int(time.time()-start)),' seconds'
    except:
        print '\t-E: Annotating the islands to the gff and table files has failed'
        #annotate_isles_to_gff(islands, src_seq, src_seq_translated, src_seq_translated_weights, isl_gff_f)
        return 'FAILED'

    print '\nThe isle identification stage is done'

    return 'FOUND '+str(isl_len)+' isl'




def run_pfam_unidentifed_stage(faa_f, meta_gff_f, hmm_wghts_f, pfam_fasta_f, isl_tbl_f, isl_pfam_gff_f, isl_pfam_with_src_gff_f, pfam_out_f, pfam_filt_out_f, hmm_filt_out_f, rerun_flag = True):
    ''' This implements PFMA run on unidentified CDS contained in found islands

    To implement this stage the next sub tasks have to be done
    a. Collect the CDS to be PFAMed form islands file
    b. Prepare small FASTA file from collected CDS
    c. Running PFAM
    d. Translating CDS to PFAM and HMM profiles
    e. Annotating islands to GFF files with translation of found CDS to PFAM and HMM profiles

    Input to the function
    @faa_f - metagenome FASTA file (used to substruct smaller FASTA for PFAM)
    @meta_gff_f - metagenome source sequences (contains islands' CDS)
    @pfam_fasta_f - auxilary an input file for PFAM (used to run PFAM)
    @isl_tbl_f - island annotation file in table format (used mainly for island load/save in scripts)
    @isl_pfam_gff_f - island annotation file in GFF format containing PFAM and HMM mappings of CDS (used mainly as user output)
    @pfam_out_f - output file for PFAM
    @pfam_filt_out_f - reduced PFAM output file (used as CDS-PFAM profile mapping)
    @hmm_filt_out_f - reduced HMM output file (used as CDS-HMM profile mapping)
    @rerun_flag - boolean flag indication weather the script should be run again even if output files exist
   
    
    '''
    
    # If no need to rerun then return
    if rerun_flag == False and os.path.exists(isl_pfam_gff_f) and os.path.exists(isl_pfam_with_src_gff_f) and os.path.exists(pfam_fasta_f) and os.path.exists(pfam_out_f) and os.path.exists(pfam_filt_out_f):
        print '\t-I: Rerun flag is set to False and all the output files exist thus skipping pfam run stage'
        return 'OUTPUT EXISTS'
    
    # Remove the previous run files
    try:
        os.remove(isl_pfam_gff_f)
    except OSError:
        pass
    try:
        os.remove(isl_pfam_with_src_gff_f)
    except OSError:
        pass
    try:
        os.remove(pfam_fasta_f)
    except OSError:
        pass
    try:
        os.remove(pfam_out_f)
    except OSError:
        pass
    try:
        os.remove(pfam_filt_out_f)
    except OSError:
        pass



    print 'Starting PFAM run stage'

    # Load islands information
    print '\n\tPreparing relevant CDS for PFAM run...'
    start = time.time()
    try:
        islands = read_isle_tbl_file(isl_tbl_f)
    except:
        print "\t-E: Reading islands table has failed"
        return 'FAILED'
    
    # Load metasequence dictionary
    try:
        src_dict = read_gff_to_sorted_dict(meta_gff_f)
    except:
        print "\t-E: Reading GFF has failed"
        return 'FAILED'
    
    # Collect features for PFAM identification
    #meta_src - has all the sourses with their names. We need to take them and extract only the names of relevant cds 
    cds_for_pfam = []
    #print islands.keys()
    for meta_src in islands.keys():
        for isl in islands[meta_src]:
            for i in range(isl[0], isl[1]+1):
                #print meta_src
                #print i
                cds_for_pfam.append(src_dict[meta_src][i][0])

    print '\tFound', len(cds_for_pfam), 'CDS for pfam identification'
    print '\tPreparing relevant CDS for PFAM run has finished in', str(int(time.time()-start)), 'seconds'


    # Write FASTA file for PFAM run
    try:
        print '\n\tPreparing fasta file for PFAM run...'
        start = time.time()
        filter_fasta_file(faa_f, pfam_fasta_f, cds_for_pfam)
    
        print '\tPreparing fasta file for PFAM run has finished in', str(int(time.time()-start)), 'seconds'
    except:
        print "\t-E: Writing FASTA for PFAM has failed"
        return 'FAILED'



    # Run PFAM PFAMSCAN
    
    print '\n\tRunning pfamscan...'
    start=time.time()
    pfamscan_cmnd = "perl Pfam\pfam_scan.pl" + " -outfile "+pfam_out_f+" -fasta "+pfam_fasta_f+" -dir Pfam"
    try:
        s = subprocess.check_output(pfamscan_cmnd, stderr=subprocess.STDOUT)
        print "\tpfamscan has finished in", str(int(time.time()-start)), "seconds"
    except subprocess.CalledProcessError as e:
        print "\t-E: Looks like the pfamscan has failed. Please check error output"
        print '\t', e.output
        return 'FAILED'
        


    # Filter pfam scan results and deside on mapping
    try:
        print "\n\tpfamscan output filtering..."
        start = time.time()
        filter_pfamscan_out(pfam_out_f, pfam_filt_out_f)
                
        print "\tpfamscan output filtering has finished in", str(int(time.time()-start)), "seconds"
    except:
        print "\t-E: PFAM output filtering has failed"
        return 'FAILED'



    # Reading the PFAM mapping
    try:
        print "\n\tReading filtered mapping..."
        start = time.time()
        pfam_mapping = read_pfam_filtered_mapping(pfam_filt_out_f)
        hmm_mapping = read_filtered_mapping(hmm_filt_out_f)
                
        print "\tReading filtered mapping has finished in", str(int(time.time()-start)), "seconds"
    except:
        print "\t-E: Reading PFAM filtered mapping has failed"
        return 'FAILED'

    # Reading the weights file (for gff annotation)
    print '\n\tReading hmm profile weights file...'
    start = time.time()
    weights_dict = {}
    if not os.path.exists(hmm_wghts_f):
        print '\n\tNo HMM profile weight file found. Using default weight for all profiles'
    else:
        weights_dict = read_hmm_weights(hmm_wghts_f)
        print '\n\tFound ', len(weights_dict),'profile weights. For not found profiles default weight will be used'
    weights_dict_keys = weights_dict.keys()
    print '\tReading hmm profile weights file has finished in', str(int(time.time()-start)), 'seconds'


    # Translate metasequences with new PFAM mapping
    print '\n\tTranslating meta source sequencs to HMM and PFAM profiles...'
    start = time.time()
    src_seq_translated = {}
    src_seq_translated_bin = {}
    src_seq_translated_weights = {}
    
    for name, cds in src_dict.iteritems():
        (mapped, binary) = map_cds_features_to_profiles(cds, hmm_mapping, 1, pfam_mapping, 2)
        src_seq_translated[name] = mapped
        src_seq_translated_bin[name] = binary
        weights = []
        for i in range(len(mapped)):
            if mapped[i] in weights_dict_keys:
                weights.append(weights_dict[mapped[i]])
            else:
                weights.append(float(binary[i]))
                
        src_seq_translated_weights[name] = weights
    
    print '\tTranslating meta source sequencs to HMM and PFAM profiles has finished in', str(int(time.time()-start)), 'seconds'



    # Annotating islands GFF with updated mapping
    try:
        print '\n\tAnnotating the islands to the gff output file...'
        start = time.time()
        annotate_isles_to_gff(islands, src_dict, src_seq_translated, src_seq_translated_weights, isl_pfam_gff_f)
        annotate_isles_incl_src_to_gff(islands, src_dict, src_seq_translated, src_seq_translated_weights, isl_pfam_with_src_gff_f)
        print '\tAnnotating the islands to the gff output file has finished in', str(int(time.time()-start)), 'seconds'

    except:
        print "\t-E: Annotation of PFAM annotataed islands to GFF has failed"
        return 'FAILED'
    
    print '\nThe PFAM stage is finished'

    return 'DONE'



