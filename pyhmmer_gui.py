# Project:  pyhmmer/islander
# Date:     25/06/2016
# Author:   Anton Shifman [santonsh[at]gmail.com]
# Supervisor: Noam Grinmberg
# File version: 1.0
#
# Description: This file implements pyhmmer/islander GUI. This GUI can be used to
# configure and/or run islander batches



import sys
from PyQt4 import QtGui
from PyQt4 import QtCore
from pyhmmer_io_aux import get_metaseq_fasta_size
from pyhmmer_io_aux import examine_gff
from pyhmmer_io_aux import hmm_profiles_number
from pyhmmer_io_aux import auto_detect_file
from pyhmmer_io_aux import open_run_config
from pyhmmer_io_aux import read_hmm_weights

from pyhmmer_stages import run_stages_for_all_metagenomes
from pyhmmer_stages import run_all_preblast_stages_for_single_metagenome
from pyhmmer_stages import run_blast_search_unidentified_stage

import pickle
import json
import os

class Window(QtGui.QDialog):
    
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        ### Initiating config file

        with open('def_config.json') as config_file:    
            def_config = json.load(config_file)
            
        self.config = {}
        self.config['meta_dir'] = ''
        self.config['faa_file'] = ''
        self.config['gff_file'] = ''
        self.config['hmm_file'] = ''
        self.config['hmm_wght_file'] = ''
        self.config['min_len_th'] = def_config['min_len_th']
        self.config['min_avg_wght_th'] = def_config['min_avg_wght_th']
        self.config['min_eval_th'] = def_config['min_eval_th']

        

        # # # # # # # # # # # # # # # # # # #
        #
        # 'Configuration' window construction
        #
        # # # # # # # # # # # # # # # # # # #
           
        
        ### Separators
        # Metagenome directory separator
        self.sep_name0 = QtGui.QHBoxLayout()
        self.h_sep0 =  QtGui.QFrame()
        self.h_sep0.setFrameStyle(QtGui.QFrame.HLine | QtGui.QFrame.Sunken)
        self.h_sep0.setSizePolicy(QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Expanding)
        self.sep_name0.addWidget(QtGui.QLabel("Metagenome directory"))
        self.sep_name0.addWidget(self.h_sep0)
        
        # Input files separator
        self.sep_name1 = QtGui.QHBoxLayout()
        self.h_sep1 =  QtGui.QFrame()
        self.h_sep1.setFrameStyle(QtGui.QFrame.HLine | QtGui.QFrame.Sunken)
        self.h_sep1.setSizePolicy(QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Expanding)
        self.sep_name1.addWidget(QtGui.QLabel("Input files"))
        self.sep_name1.addWidget(self.h_sep1)

        # Thresholds separator
        self.sep_name2 = QtGui.QHBoxLayout()
        self.h_sep2 =  QtGui.QFrame()
        self.h_sep2.setFrameStyle(QtGui.QFrame.HLine | QtGui.QFrame.Sunken)
        self.h_sep2.setSizePolicy(QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Expanding)
        self.sep_name2.addWidget(QtGui.QLabel("Thresholds"))
        self.sep_name2.addWidget(self.h_sep2)

        # Left-right separator
        self.sep_name3 = QtGui.QHBoxLayout()
        self.v_sep1 =  QtGui.QFrame()
        self.v_sep1.setFrameStyle(QtGui.QFrame.VLine | QtGui.QFrame.Sunken)
        self.v_sep1.setSizePolicy(QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Expanding)
        self.sep_name3.addWidget(self.v_sep1)

        ### Metagenome directory
        self.meta_dir = QtGui.QHBoxLayout()
        self.meta_d_name = QtGui.QLineEdit("Please select metagenome directory")
        self.meta_d_btn = QtGui.QPushButton("Open")
        self.meta_dir.addWidget(self.meta_d_name)
        self.meta_dir.addWidget(self.meta_d_btn)
        
        ### Input files options
        # Fasta file
        self.faa_file = QtGui.QHBoxLayout()
        self.faa_f_name = QtGui.QLineEdit("Please select faa file")
        self.faa_f_btn = QtGui.QPushButton("Open")
        self.faa_file.addWidget(self.faa_f_name)
        self.faa_file.addWidget(self.faa_f_btn)
        # Gff file
        self.gff_stats = {}
        self.gff_file = QtGui.QHBoxLayout()
        self.gff_f_name = QtGui.QLineEdit("Please select gff file")
        self.gff_f_btn = QtGui.QPushButton("Open")
        self.gff_file.addWidget(self.gff_f_name)
        self.gff_file.addWidget(self.gff_f_btn)
        # HMMER profiles file
        self.hmm_file = QtGui.QHBoxLayout()
        self.hmm_f_name = QtGui.QLineEdit("Please select hmmer profile file")
        self.hmm_f_btn = QtGui.QPushButton("Open")
        self.hmm_file.addWidget(self.hmm_f_name)
        self.hmm_file.addWidget(self.hmm_f_btn)
        # HMMER profiles weights file
        self.hmm_w_file = QtGui.QHBoxLayout()
        self.hmm_w_f_name = QtGui.QLineEdit("Please select hmmer profile weights file")
        self.hmm_w_f_btn = QtGui.QPushButton("Open")
        self.hmm_w_file.addWidget(self.hmm_w_f_name)
        self.hmm_w_file.addWidget(self.hmm_w_f_btn)

        # Gluing together
        self.input_files_opts = QtGui.QVBoxLayout()
        self.input_files_opts.addLayout(self.faa_file)
        self.input_files_opts.addLayout(self.gff_file)
        self.input_files_opts.addLayout(self.hmm_file)
        self.input_files_opts.addLayout(self.hmm_w_file)
        
        ### Thresholds options
        # Minimal island length
        self.min_len_t = QtGui.QLabel('Min isle length (CDS number)')
        self.min_len_th = QtGui.QLineEdit(str(self.config['min_len_th']))
        self.min_len_th.setFixedWidth(30)

        # Minimal island average weight
        self.min_avg_wght_t = QtGui.QLabel('Min avg isle weight')
        self.min_avg_wght_th = QtGui.QLineEdit(str(self.config['min_avg_wght_th']))
        self.min_avg_wght_th.setFixedWidth(30)

        # Minimal island e-value
        self.min_eval_t = QtGui.QLabel('Min isle e-value')
        self.min_eval_th = QtGui.QLineEdit(str(self.config['min_eval_th']))
        self.min_eval_th.setFixedWidth(30)

        # Gluing together
        self.thresholds = QtGui.QGridLayout()
        
        self.thresholds.addWidget(self.min_len_t, 1, 1)
        self.thresholds.addWidget(self.min_avg_wght_t, 2, 1)
        self.thresholds.addWidget(self.min_eval_t,  3, 1)

        self.thresholds.addWidget(self.min_len_th, 1, 2)
        self.thresholds.addWidget(self.min_avg_wght_th, 2, 2)
        self.thresholds.addWidget(self.min_eval_th,  3, 2)


        ### Button panel
        #
        self.load_btn = QtGui.QPushButton("Load")
        self.load_btn.setMinimumHeight(50)
        self.save_btn = QtGui.QPushButton("Save")
        self.save_btn.setMinimumHeight(50)
        self.defaults_btn = QtGui.QPushButton("Defaults")
        self.defaults_btn.setMinimumHeight(50)
        self.autodetect_btn = QtGui.QPushButton("Autodetect files")
        self.autodetect_btn.setMinimumHeight(50)
        self.configure_all_btn = QtGui.QPushButton("Autoconfigure all\n meta dirs")
        self.configure_all_btn.setMinimumHeight(50)

        # Gluing together
        self.btn_grid = QtGui.QGridLayout()
        self.btn_grid.addWidget(self.load_btn, 1,1)
        self.btn_grid.addWidget(self.save_btn, 4,1)
        self.btn_grid.addWidget(self.defaults_btn, 3,1)
        self.btn_grid.addWidget(self.autodetect_btn, 2,1)
        self.btn_grid.addWidget(self.configure_all_btn, 5,1)
        

        # Glueing together again
        
        self.config_window = QtGui.QGridLayout()
        self.config_window.addLayout(self.sep_name0, 1, 1)
        self.config_window.addLayout(self.meta_dir, 2, 1)
        self.config_window.addLayout(self.sep_name1, 3, 1)
        self.config_window.addLayout(self.input_files_opts, 4, 1)
        self.config_window.addLayout(self.sep_name2, 5, 1)
        self.config_window.addLayout(self.thresholds, 6, 1)
        self.config_window.addLayout(self.sep_name3, 1, 2, 6, 2)
        self.config_window.addLayout(self.btn_grid, 1, 4, 6, 4)



        # Button functionality
        def choose_meta():
            self.meta_d_btn.setText('Wait...')
            ret = QtGui.QFileDialog.getExistingDirectory (self, "Choose Meta Directory", "Input\Sequences")
            if len(ret)>0:
                self.config['meta_dir'] = str(ret)
                self.meta_d_name.setText(ret)
                text = 'Cool. Metagenome directory is set to ' + ret
                self.meta_d_name.setStyleSheet("color: rgb(0, 180, 0);")
                print text
            self.meta_d_btn.setText('Open')
            check_btn_state()
            
        def choose_faa():
            self.faa_f_btn.setText('Wait...')
            ret = QtGui.QFileDialog.getOpenFileName(self, "Choose Fasta File", "Input\Sequences", "All Files (*)")
            if len(ret)>0:
                self.faa_f_name.setText(ret)
                records_number = get_metaseq_fasta_size(self.faa_f_name.text())
                if records_number<1:
                    self.config['faa_file'] = ''
                    text = 'The file doesnt contain fasta records. Please try again'
                    self.faa_f_name.setStyleSheet("color: rgb(255, 0, 0);")
                else:
                    self.config['faa_file'] = str(ret)
                    text = 'Cool. Found ' + str(records_number) + ' CDAs in this metagenome'
                    self.faa_f_name.setStyleSheet("color: rgb(0, 180, 0);")
                print text
            self.faa_f_btn.setText('Open')
            check_btn_state()
            
        def choose_gff():
            self.gff_f_btn.setText('Wait...')
            ret = QtGui.QFileDialog.getOpenFileName(self, "Choose Gff file", "Input\Sequences", "All Files (*)")
            if len(ret)>0:
                self.gff_f_name.setText(ret)
                gff_stats = examine_gff(self.gff_f_name.text())
                records_number = len(gff_stats)
                if records_number<1:
                    self.config['gff_file'] = ''
                    text = 'No source sequences found. Please try again'
                    self.gff_f_name.setStyleSheet("color: rgb(255, 0, 0);")
                else:
                    self.config['gff_file'] = str(ret)
                    text = 'Cool. Found ' + str(records_number) + ' source sequences in this metagenome'
                    self.gff_f_name.setStyleSheet("color: rgb(0, 180, 0);")
                    self.gff_stats = gff_stats
                print text
            self.gff_f_btn.setText('Open')
            check_btn_state()
            
        def choose_hmm():
            self.hmm_f_btn.setText('Wait...')
            ret = QtGui.QFileDialog.getOpenFileName(self, "Choose HMMER profiles file", "Input\HMMs", "All Files (*)")
            if len(ret)>0:
                self.hmm_f_name.setText(ret)
                records_number = hmm_profiles_number(self.hmm_f_name.text())
                if records_number<1:
                    self.config['hmm_file'] = ''
                    text = 'No hmm profiles found. Please try again'
                    self.hmm_f_name.setStyleSheet("color: rgb(255, 0, 0);")
                else:
                    self.config['hmm_file'] = str(ret)
                    text = 'Cool. Found ' + str(records_number) + ' HMM profiles in the file'
                    self.hmm_f_name.setStyleSheet("color: rgb(0, 180, 0);")
                print text
            self.hmm_f_btn.setText('Open')
            check_btn_state()

        def choose_weights():
            self.hmm_w_f_btn.setText('Wait...')
            ret = QtGui.QFileDialog.getOpenFileName(self, "Choose HMMER profile weights file", "Input\HMMs", "All Files (*)")
            if len(ret)>0:
                self.hmm_w_f_name.setText(ret)
                print self.hmm_w_f_name.text()
                weights = read_hmm_weights(self.hmm_w_f_name.text())
                if len(weights)<1:
                    self.config['hmm_wght_file'] = ''
                    text = 'No hmm profile weights found. Please try again'
                    self.hmm_w_f_name.setStyleSheet("color: rgb(255, 0, 0);")
                else:
                    self.config['hmm_wght_file'] = str(ret)
                    text = 'Cool. Found ' + str(len(weights)) + ' HMM profile wieghts in the file. The HMM profiles not mentioned in this file will get a default weight of 1'
                    self.hmm_w_f_name.setStyleSheet("color: rgb(0, 180, 0);")
                print text
            self.hmm_w_f_btn.setText('Open')
            check_btn_state()

        def set_min_len_th():
            val = self.min_len_th.text()
            try:
                val = int(val)
            except:
                print 'Wrong value choosen'
                self.config['min_len_th'] = ''
                self.min_len_th.setStyleSheet("color: rgb(255, 0, 0);")
                check_btn_state()
                return
            if val>0:
                self.config['min_len_th'] = val
                self.min_len_th.setStyleSheet("color: rgb(0, 0, 0);")
            else:
                print 'Wrong value choosen'
                self.config['min_len_th'] = ''
                self.min_len_th.setStyleSheet("color: rgb(255, 0, 0);")
            check_btn_state()

        def set_min_avg_wght_th():
            val = self.min_avg_wght_th.text()
            try:
                val = float(val)
            except:
                print 'Wrong value choosen'
                self.config['min_avg_wght_th'] = ''
                self.min_avg_wght_th.setStyleSheet("color: rgb(255, 0, 0);")
                check_btn_state()
                return
            if val>=0:
                self.config['min_avg_wght_th'] = val
                self.min_avg_wght_th.setStyleSheet("color: rgb(0, 0, 0);")
            else:
                print 'Wrong value choosen'
                self.config['min_avg_wght_th'] = ''
                self.min_avg_wght_th.setStyleSheet("color: rgb(255, 0, 0);")
            check_btn_state()

        def set_min_eval_th():
            val = self.min_eval_th.text()
            try:
                val = float(val)
            except:
                print 'Wrong value choosen'
                self.config['min_eval_th'] = ''
                self.min_eval_th.setStyleSheet("color: rgb(255, 0, 0);")
                check_btn_state()
                return
            if val>=0:
                self.config['min_eval_th'] = val
                self.min_eval_th.setStyleSheet("color: rgb(0, 0, 0);")
            else:
                print 'Wrong value choosen'
                self.config['min_eval_th'] = ''
                self.min_eval_th.setStyleSheet("color: rgb(255, 0, 0);")
            check_btn_state()

        def auto_detect_meta_files():
            # This function tries to autodetect faa and gff files and bring them to configuration
            # first the faa file
            if self.config['meta_dir'] == '':
                print 'Please choose metagenome directory. I dont know where to look for faa and gff'
                return
            else:
                self.autodetect_btn.setText('Wait...')
                faa_file = auto_detect_file(self.config['meta_dir'], '.faa')
                if faa_file == '':
                    self.faa_f_name.setText("Please select faa file")
                    self.faa_f_name.setStyleSheet("color: rgb(0, 0, 0);")
                else:
                    self.faa_f_name.setText(faa_file)
                    records_number = get_metaseq_fasta_size(self.faa_f_name.text())

                    if records_number<1:
                        self.config['faa_file'] = ''
                        text = 'The file doesnt contain fasta records. Please try again'
                        self.faa_f_name.setStyleSheet("color: rgb(255, 0, 0);")
                    else:
                        self.config['faa_file'] = faa_file
                        text = 'Cool. Found ' + str(records_number) + ' CDAs in this metagenome'
                        self.faa_f_name.setStyleSheet("color: rgb(0, 180, 0);")
                    print text

            # now the gff file
                gff_file = auto_detect_file(self.config['meta_dir'], '.gff')
                if gff_file == '':
                    self.gff_f_name.setText("Please select gff file")
                    self.gff_f_name.setStyleSheet("color: rgb(0, 0, 0);")
                else:
                    self.gff_f_name.setText(gff_file)
                    gff_stats = examine_gff(self.gff_f_name.text())
                    records_number = len(gff_stats)
                    
                    if records_number<1:
                        self.config['gff_file'] = ''
                        text = 'The file doesnt contain source sequences. Please try again'
                        self.gff_f_name.setStyleSheet("color: rgb(255, 0, 0);")
                    else:
                        self.config['gff_file'] = gff_file
                        text = 'Cool. Found ' + str(records_number) + ' source sequences in this metagenome'
                        self.gff_f_name.setStyleSheet("color: rgb(0, 180, 0);")
                    print text
                    
                self.autodetect_btn.setText('Autodetect files')
                check_btn_state()
                
        def check_btn_state():
            # This auxilary function will decide on button availability based on configuration state
            config = self.config
            if config['meta_dir'] == '':
                self.load_btn.setEnabled(False)
                self.autodetect_btn.setEnabled(False)
                
                self.run_HMMER_btn.setEnabled(False)
                self.run_islands_btn.setEnabled(False)
                self.run_PFAM_btn.setEnabled(False)
                self.run_all_btn.setEnabled(False)
            else:
                self.load_btn.setEnabled(True)
                self.autodetect_btn.setEnabled(True)
                if os.path.exists(os.path.join(config['meta_dir'], 'config.json')):
                    self.run_HMMER_btn.setEnabled(True)
                    self.run_islands_btn.setEnabled(True)
                    self.run_PFAM_btn.setEnabled(True)
                    self.run_all_btn.setEnabled(True)
                    

            if config['meta_dir'] == '' or config['faa_file'] == '' or config['gff_file'] == '' or config['hmm_file'] == '' or config['min_len_th'] == '' or config['min_avg_wght_th'] == '' or config['min_eval_th'] == '':
                self.save_btn.setEnabled(False)
            else:
                self.save_btn.setEnabled(True)

            if config['hmm_file'] == '' or config['min_len_th'] == '' or config['min_avg_wght_th'] == '' or config['min_eval_th'] == '':
                self.configure_all_btn.setEnabled(False)
            else:
                self.configure_all_btn.setEnabled(True)    
                
        def update_fields_with_config():
            # Auxilary function to be used with configuration loading functions
            if self.config['faa_file'] != "":
                self.faa_f_name.setText(self.config['faa_file'])
                self.faa_f_name.setStyleSheet("color: rgb(0, 180, 0);")
            else:
                self.faa_f_name.setText("Please select faa file")
                self.faa_f_name.setStyleSheet("color: rgb(0, 0, 0);")
            if self.config['gff_file'] != "":
                self.gff_f_name.setText(self.config['gff_file'])
                self.gff_f_name.setStyleSheet("color: rgb(0, 180, 0);")
            else:
                self.gff_f_name.setText("Please select gff file")
                self.gff_f_name.setStyleSheet("color: rgb(0, 0, 0);")
            if self.config['hmm_file'] != "":
                self.hmm_f_name.setText(self.config['hmm_file'])
                self.hmm_f_name.setStyleSheet("color: rgb(0, 180, 0);")
            else:
                self.hmm_f_name.setText("Please select hmmer profile file")
                self.hmm_f_name.setStyleSheet("color: rgb(0, 0, 0);")
            if self.config['hmm_wght_file'] != "":
                self.hmm_w_f_name.setText(self.config['hmm_wght_file'])
                self.hmm_w_f_name.setStyleSheet("color: rgb(0, 180, 0);")
            else:
                self.hmm_w_f_name.setText("Please select hmmer profile weights file")
                self.hmm_w_f_name.setStyleSheet("color: rgb(0, 0, 0);")
            self.min_len_th.setText(str(self.config['min_len_th']))
            self.min_len_th.setStyleSheet("color: rgb(0, 0, 0);")
            self.min_avg_wght_th.setText(str(self.config['min_avg_wght_th']))
            self.min_avg_wght_th.setStyleSheet("color: rgb(0, 0, 0);")
            self.min_eval_th.setText(str(self.config['min_eval_th']))
            self.min_eval_th.setStyleSheet("color: rgb(0, 0, 0);")
            
        def restore_defaults():
            meta_dir_bak = self.config['meta_dir']
            with open('def_config.json') as config_file:    
                self.config = json.load(config_file)
            self.config['meta_dir'] = meta_dir_bak
            update_fields_with_config()
            check_btn_state()
            print 'Default configuration restored'
            

        def save_config():
            if self.config['meta_dir'] == '':
                print 'Please choose metagenome directory. I dont know where to save the config file'
            elif self.config['faa_file'] == '':
                print 'Fasta file wasnt configured. Please choose one'
            elif self.config['gff_file'] == '':
                print 'Gff file wasnt configured. Please choose one'
            elif self.config['hmm_file'] == '':
                print 'HMM file wasnt configured. Please choose one'
            elif (self.config['min_len_th'] == '') or (self.config['min_avg_wght_th'] == '') or (self.config['min_eval_th'] == ''):
                print 'One or more thresholds wasnt set explicitly. Please define all the thresholds'    
            else:
                try:
                    filename = self.config['meta_dir']+'\config.json'
                    with open(filename , 'w') as config_file:    
                        json.dump(self.config, config_file, indent=4, sort_keys=True)
                        print 'The config file was saved to '+filename
                
                except:
                    print 'Something wrong. Cant save the config file'
                    print sys.exc_info()[0]
                
        def load_config():
            if self.config['meta_dir'] == '':
                print 'Please choose metagenome directory. I dont know what config file to load'
                return
            else:
                filename = self.config['meta_dir']+'\config.json'
                
            meta_dir_bak = self.config['meta_dir']
            
            with open(filename, 'r') as config_file:    
                self.config = json.load(config_file)
                self.config['meta_dir'] = meta_dir_bak

            update_fields_with_config()

            check_btn_state()

        def run_auto_configurator():
            # This will run the autoconfigurator script
            # for all the metagenomes in the sequence directory
            print '\nRunning auto configurator'
            from pyhmmer_auto_configurator import autoconfig_all
            args = ['--hmm_file', self.config['hmm_file'], '--hmmw_file', self.config['hmm_wght_file'],
                    '--eval', self.config['min_eval_th'], '--len', self.config['min_len_th'],
                    '--avgw', str(self.config['min_avg_wght_th'])]
            autoconfig_all(args)
            print '\tI-Finished running autoconfigurator'

        # Connect functionality to buttons and fields
        self.connect(self.meta_d_btn, QtCore.SIGNAL("clicked()"), choose_meta)
        self.connect(self.faa_f_btn, QtCore.SIGNAL("clicked()"), choose_faa)
        self.connect(self.gff_f_btn, QtCore.SIGNAL("clicked()"), choose_gff)
        self.connect(self.hmm_f_btn, QtCore.SIGNAL("clicked()"), choose_hmm)
        self.connect(self.hmm_w_f_btn, QtCore.SIGNAL("clicked()"), choose_weights)
        self.connect(self.load_btn, QtCore.SIGNAL("clicked()"), load_config)
        self.connect(self.save_btn, QtCore.SIGNAL("clicked()"), save_config)
        self.connect(self.defaults_btn, QtCore.SIGNAL("clicked()"), restore_defaults)
        self.connect(self.autodetect_btn, QtCore.SIGNAL("clicked()"), auto_detect_meta_files)
        self.connect(self.configure_all_btn, QtCore.SIGNAL("clicked()"), run_auto_configurator)

        self.connect(self.min_len_th, QtCore.SIGNAL("editingFinished()"), set_min_len_th)
        self.connect(self.min_avg_wght_th, QtCore.SIGNAL("editingFinished()"), set_min_avg_wght_th)
        self.connect(self.min_eval_th, QtCore.SIGNAL("editingFinished()"), set_min_eval_th)

        # # # # # # # # # # # # # # # # # # #
        #
        # 'Configuration' window construction end
        #
        # # # # # # # # # # # # # # # # # # #



        # # # # # # # # # # # # # # # # # # #
        #
        # 'Run' window construction
        #
        # # # # # # # # # # # # # # # # # # #

        # Run separator
        self.sep_name4 = QtGui.QHBoxLayout()
        self.h_sep4 =  QtGui.QFrame()
        self.h_sep4.setFrameStyle(QtGui.QFrame.HLine | QtGui.QFrame.Sunken)
        self.h_sep4.setSizePolicy(QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Expanding)
        self.sep_name4.addWidget(QtGui.QLabel("Run metagenome stages"))
        self.sep_name4.addWidget(self.h_sep4)

        # Run all configured genomes separator
        self.sep_name5 = QtGui.QHBoxLayout()
        self.h_sep5 =  QtGui.QFrame()
        self.h_sep5.setFrameStyle(QtGui.QFrame.HLine | QtGui.QFrame.Sunken)
        self.h_sep5.setSizePolicy(QtGui.QSizePolicy.Minimum,QtGui.QSizePolicy.Expanding)
        self.sep_name5.addWidget(QtGui.QLabel("Run all configured metagenomes stages"))
        self.sep_name5.addWidget(self.h_sep5)

        ### Buttons
        self.run_HMMER_btn = QtGui.QPushButton("Run HMMER")
        self.run_HMMER_btn.setMinimumHeight(50)
        self.run_islands_btn = QtGui.QPushButton("Identify islands")
        self.run_islands_btn.setMinimumHeight(50)
        self.run_PFAM_btn = QtGui.QPushButton("Run PFAM")
        self.run_PFAM_btn.setMinimumHeight(50)
        self.run_all_btn = QtGui.QPushButton("Run all stages")
        self.run_all_btn.setMinimumHeight(50)

        self.multi_run_HMMER_btn = QtGui.QPushButton("Run HMMER")
        self.multi_run_HMMER_btn.setMinimumHeight(50)
        self.multi_run_islands_btn = QtGui.QPushButton("Identify islands")
        self.multi_run_islands_btn.setMinimumHeight(50)
        self.multi_run_PFAM_btn = QtGui.QPushButton("Run PFAM")
        self.multi_run_PFAM_btn.setMinimumHeight(50)
        self.multi_run_BLAST_btn = QtGui.QPushButton("Run BLAST")
        self.multi_run_BLAST_btn.setMinimumHeight(50)
        self.multi_run_all_btn = QtGui.QPushButton("Run all stages")
        self.multi_run_all_btn.setMinimumHeight(50)

        # Building rerun flag checkboxes
        self.rerun_flag = QtGui.QCheckBox()
        self.rerun_flag.setText('Re-run finished')

        self.rerun_flag_all = QtGui.QCheckBox()
        self.rerun_flag_all.setText('Re-run finished')
        
        # Building the button panel
        self.run_btn_grid1 = QtGui.QGridLayout() 
        self.run_btn_grid1.addWidget(self.run_HMMER_btn,         1,1)
        self.run_btn_grid1.addWidget(self.run_islands_btn,       1,2)
        self.run_btn_grid1.addWidget(self.run_PFAM_btn,          1,3)
        self.run_btn_grid1.addWidget(QtGui.QLabel(),             1,4)
        self.run_btn_grid1.addWidget(self.run_all_btn,           1,5)

        self.run_btn_grid2 = QtGui.QGridLayout()
        self.run_btn_grid2.addWidget(self.multi_run_HMMER_btn,   1,1)
        self.run_btn_grid2.addWidget(self.multi_run_islands_btn, 1,2)
        self.run_btn_grid2.addWidget(self.multi_run_PFAM_btn,    1,3)
        self.run_btn_grid2.addWidget(self.multi_run_BLAST_btn,   1,4)        
        self.run_btn_grid2.addWidget(self.multi_run_all_btn,     1,5)
        
        # Glueing togrther run window
        self.run_window = QtGui.QVBoxLayout()
        self.run_window.addLayout(self.sep_name4)
        self.run_window.addWidget(self.rerun_flag)
        self.run_window.addLayout(self.run_btn_grid1)
        self.run_window.addLayout(self.sep_name5)
        self.run_window.addWidget(self.rerun_flag_all)
        self.run_window.addLayout(self.run_btn_grid2)

        # Dynamic function generator for stage button calbacks
        def make_run_stage(stage):

            if stage == 'hmmer':
                stages = { 'hmmer':True, 'islands':False, 'pfam':False, 'pre_blast':False, 'blast':False }
            if stage == 'islands':
                stages = { 'hmmer':False, 'islands':True, 'pfam':False, 'pre_blast':False, 'blast':False }
            if stage == 'pfam':
                stages = { 'hmmer':False, 'islands':False, 'pfam':True, 'pre_blast':True, 'blast':False }
            if stage == 'all':
                stages = { 'hmmer':True, 'islands':True, 'pfam':True, 'pre_blast':True, 'blast':True }

            
            
            def run_stage():
                
                c = open_run_config('file_dir_config.json')
            
                meta = os.path.relpath(self.config['meta_dir'], os.path.join(os.getcwd(), c['gen_input_d']))

                rerun_flag = self.rerun_flag.isChecked()
                
                config_f = os.path.join(os.getcwd(), c['gen_input_d'], meta, c['gen_config_f'])
                
                run_c = open_run_config(config_f)
                faa_f =         run_c['faa_file']
                meta_gff_f =    run_c['gff_file']
                hmm_prfl_f =    run_c['hmm_file']
                hmm_wghts_f =   run_c['hmm_wght_file']
                len_th =        run_c['min_len_th']
                e_th =          run_c['min_eval_th']
                wght_th =       run_c['min_avg_wght_th']

                if not os.path.exists(os.path.join(os.getcwd(), c['gen_output_d'], meta)):
                    os.makedirs(os.path.join(os.getcwd(), c['gen_output_d'], meta))

                hmmscan_fasta_f =       os.path.join(os.getcwd(), c['gen_output_d'], meta, c['gen_hmmscan_fasta_f'])
                hmm_out_f =             os.path.join(os.getcwd(), c['gen_output_d'], meta, c['gen_hmm_out_f'])
                hmm_filt_out_f =        os.path.join(os.getcwd(), c['gen_output_d'], meta, c['gen_hmm_filt_out_f'])
                isl_gff_f =             os.path.join(os.getcwd(), c['gen_output_d'], meta, c['gen_isl_gff_f'])
                isl_with_src_gff_f =    os.path.join(os.getcwd(), c['gen_output_d'], meta, c['gen_isl_with_src_gff_f'])
                isl_tbl_f =             os.path.join(os.getcwd(), c['gen_output_d'], meta, c['gen_isl_tbl_f'])
                isl_pfam_gff_f =        os.path.join(os.getcwd(), c['gen_output_d'], meta, c['gen_isl_pfam_gff_f'])
                isl_pfam_with_src_gff_f=os.path.join(os.getcwd(), c['gen_output_d'], meta, c['gen_isl_pfam_with_src_gff_f'])
                pfam_fasta_f =          os.path.join(os.getcwd(), c['gen_output_d'], meta, c['gen_pfam_fasta_f'])
                pfam_out_f =            os.path.join(os.getcwd(), c['gen_output_d'], meta, c['gen_pfam_out_f'])
                pfam_filt_out_f =       os.path.join(os.getcwd(), c['gen_output_d'], meta, c['gen_pfam_filt_out_f'])     
                blast_fasta_f =         os.path.join(os.getcwd(), c['gen_output_d'], meta, c['gen_blast_fasta_f'])

                run_all_preblast_stages_for_single_metagenome(stages,
                                                              faa_f,
                                                              meta_gff_f,
                                                              hmmscan_fasta_f,
                                                              hmm_prfl_f,
                                                              hmm_wghts_f,
                                                              hmm_out_f,
                                                              hmm_filt_out_f,
                                                              isl_gff_f,
                                                              isl_with_src_gff_f,
                                                              isl_tbl_f,
                                                              isl_pfam_gff_f,
                                                              isl_pfam_with_src_gff_f,
                                                              len_th,
                                                              e_th,
                                                              wght_th,
                                                              pfam_fasta_f,
                                                              pfam_out_f,
                                                              pfam_filt_out_f,
                                                              blast_fasta_f,
                                                              rerun_flag)

            return run_stage

        # Dynamic function generator for stage button calbacks
        def make_multi_run_stage(stage):
            
            if stage == 'hmmer':
                stages = { 'hmmer':True, 'islands':False, 'pfam':False, 'pre_blast':False, 'blast':False }
            if stage == 'islands':
                stages = { 'hmmer':False, 'islands':True, 'pfam':False, 'pre_blast':False, 'blast':False }
            if stage == 'pfam':
                stages = { 'hmmer':False, 'islands':False, 'pfam':True, 'pre_blast':False, 'blast':False }
            if stage == 'blast':
                stages = { 'hmmer':False, 'islands':False, 'pfam':False, 'pre_blast':True, 'blast':True }
            if stage == 'all':
                stages = { 'hmmer':True, 'islands':True, 'pfam':True, 'pre_blast':True, 'blast':True }    

            c = open_run_config('file_dir_config.json')
            
            def run_stage():

                rerun_flag = self.rerun_flag_all.isChecked()
                
                run_stages_for_all_metagenomes(stages,
                                               c['gen_config_f'],
                                               c['gen_input_d'],
                                               c['gen_output_d'],
                                               c['gen_hmmscan_fasta_f'],
                                               c['gen_hmm_out_f'],
                                               c['gen_hmm_filt_out_f'],
                                               c['gen_isl_tbl_f'],
                                               c['gen_isl_gff_f'],
                                               c['gen_isl_with_src_gff_f'],
                                               c['gen_isl_pfam_gff_f'],
                                               c['gen_isl_pfam_with_src_gff_f'],
                                               c['gen_pfam_fasta_f'],
                                               c['gen_pfam_out_f'],
                                               c['gen_pfam_filt_out_f'],
                                               c['gen_blast_fasta_f'],
                                               c['gen_common_d'],
                                               c['gen_combined_metagenome_fasta_f'],
                                               c['gen_combined_nonidentified_fasta_f'],
                                               c['gen_blast_db_f'],
                                               c['gen_blast_out_f'],
                                               c['gen_in_island_CDS_lisf_f'],
                                               c['gen_unid_out_f'],
                                               rerun_flag)

            return run_stage


        # Connecting buttons to functionality
        self.connect(self.run_HMMER_btn,   QtCore.SIGNAL("clicked()"), make_run_stage('hmmer'))
        self.connect(self.run_islands_btn, QtCore.SIGNAL("clicked()"), make_run_stage('islands'))
        self.connect(self.run_PFAM_btn,    QtCore.SIGNAL("clicked()"), make_run_stage('pfam'))
        self.connect(self.run_all_btn,     QtCore.SIGNAL("clicked()"), make_run_stage('all'))

        self.connect(self.multi_run_HMMER_btn,   QtCore.SIGNAL("clicked()"), make_multi_run_stage('hmmer'))
        self.connect(self.multi_run_islands_btn, QtCore.SIGNAL("clicked()"), make_multi_run_stage('islands'))
        self.connect(self.multi_run_PFAM_btn,    QtCore.SIGNAL("clicked()"), make_multi_run_stage('pfam'))
        self.connect(self.multi_run_BLAST_btn,   QtCore.SIGNAL("clicked()"), make_multi_run_stage('blast'))
        self.connect(self.multi_run_all_btn,     QtCore.SIGNAL("clicked()"), make_multi_run_stage('all'))



        # # # # # # # # # # # # # # # # # # #
        #
        # 'Run' window construction end
        #
        # # # # # # # # # # # # # # # # # # #



        # Disabling buttons
        check_btn_state()

        # Set the layout
        self.layout = QtGui.QVBoxLayout()  
        self.layout.addLayout(self.config_window)
        self.layout.addLayout(self.run_window)
        
        self.setLayout(self.layout)

        self.resize(600,300)


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    main = Window()
    main.setWindowTitle('Phymmer configurator');
    main.show()

    sys.exit(app.exec_())
