# May need to add C:\Python and C:\Python27\Scripts to path
set PATH=%PATH%;C:\Python
set PATH=%PATH%;C:\Python27\Scripts
set PATH=%PATH%;C:\pyhmmer\hmmer3.0_windows

pip install bcbio-gff
pip install Pillow
pip install numpy

cpan CPAN
cpan -f Module::Build
cpan -f Test::Harness
cpan -f Test::Most
cpan -f CJFIELDS/BioPerl-1.6.924.tar.gz

# Copy C:\pyhmmer\Pfam\Bio to C:\Strawberry\perl\lib
xcopy C:\pyhmmer\Pfam\Bio C:\Strawberry\perl\lib\Bio /s /i