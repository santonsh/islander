
from math import factorial as f
import time

# choose a over b function
def choose(a,b):
    if b > a:
        return 1
    return f(a) / f(b) / f(a-b)

# Computes expectation value
def island_e_value(n,n_,k,k_):
    # Compute hypergeometric distribution probability
    prob = 0
    for i in range(k_, k+1):
        prob = prob + (float(choose(n_, i))*choose(n-n_,k-i)/float(choose(n,k)))
    # return expectation
    return (n-k+1)*prob

def find_minimal_e_subcluster(cluster, weights, excluded_subcluster_idx, e_th, len_th, wght_th):
    # a function used in find_islands function
    g = cluster

    # Exclude the previous sublcuster if such exists
    if len(excluded_subcluster_idx) > 0:
        for excluded in excluded_subcluster_idx:
            g[excluded[0]:excluded[1]+1] = [0 for i in g[excluded[0]:excluded[1]+1]]
    g_tag_idx = [i for i, j in enumerate(g) if j == 1]

    n = len(cluster)
    n_ = sum(cluster)

    # Check the super cluster for island length thereshold
    #if n < len_th or n_ < len_th:
    if n < len_th:
        return [[], 0, 0]

    # Initialize min eval subcluster variables
    min_exp = 100
    min_avg_wght = 0
    min_subcluster_length = 0
    min_subcluster = []
    min_subcluster_idx = []

    timeout = 300
    start = time.time()
    # Check the relevant size subclusters
    try:
        for k_ in range(1, n_+1):
            now = time.time()
            if now - start > timeout:
                print '\t-W: Island search timeout. Skipping the source sequence'
            #print k_, 'of', n_+1
            for j in range(0, n_-k_+1):
                subcluster = g[g_tag_idx[j:j+k_][0]:g_tag_idx[j:j+k_][-1]+1]
                subcluster_weights = weights[g_tag_idx[j:j+k_][0]:g_tag_idx[j:j+k_][-1]+1]
                k = len(subcluster)
                avg_wght = float(sum(subcluster_weights))/k

                # Check thresholds
                if k<len_th or avg_wght < wght_th:
                    continue

                # Calculate e-value and check thresholds again
                exp = island_e_value(n,n_,k,k_)
                if exp>e_th:
                    continue
            
                # If everything is ok lets check if the found subcluster has minimal e-value
                if min_exp == exp and min_subcluster_length<k:
                    min_subcluster = subcluster
                    min_avg_wght = avg_wght
                    min_subcluster_idx = [g_tag_idx[j:j+k_][0],g_tag_idx[j:j+k_][-1]]
                    #min_subcluster_idx = [g_tag_idx[j:j+k_][0],g_tag_idx[j:j+k_][-1]+1]
                    #if min_subcluster_idx == [6,16]:
                        #print min_subcluster, 'in +1'
                        
                elif min_exp > exp:
                    min_exp = exp
                    min_subcluster = subcluster
                    min_avg_wght = avg_wght
                    min_subcluster_idx = [g_tag_idx[j:j+k_][0],g_tag_idx[j:j+k_][-1]]
                    #if min_subcluster_idx == [6,16]:
                        #print min_subcluster, 'in reg'

        if min_exp == 100:
            return [[], 0, 0]
        else:
            return [[min_subcluster_idx[0], min_subcluster_idx[1]], min_exp, min_avg_wght]
    except:
        return [[], 0, 0]
    
def find_islands(bin_genome, weights, e_th, len_th, wght_th):
    # This returns identified islands in the sequence with corresponding
    # e-values that has at least e_thrsh e-value
    # this is used to find functional islands
    # big_genome - binary represented identification [0,0,0,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1]
    # weights - float represented CDS weights        [0,0,0,2,1,0,0,3,1,0,0,0,0,0,0,0,0,0,0,0,0,1]

    cluster = bin_genome[:]
    islands = []
    #print cluster
    dynamic_e_th = e_th
    excluded_subcluster_idx = []
    while True:
        #print 'looking'
        [subcluster_idx, e_val, avg_wght] = find_minimal_e_subcluster(cluster, weights, excluded_subcluster_idx, dynamic_e_th, len_th, wght_th)
        #print subcluster_idx
        #print 'found something'
        if len(subcluster_idx) == 0:
            break
        dynamic_e_th = min(e_th, e_val * 2)
        excluded_subcluster_idx.append(subcluster_idx)
        
        bin_rep = cluster
        start = subcluster_idx[0]
        end = subcluster_idx[1]
        #print '\tCluster finder found island! - ', start, end, e_val, avg_wght
        print '\tCluster finder found island! - s:{:4d}, e:{:4d}, e_val:{:.5f}, avg_wght:{:.2f}'.format(start, end, e_val, avg_wght)
        #print 'bin_rep', bin_rep
        islands.append([start, end, end-start+1, e_val, avg_wght*(end-start+1)])        

    return islands

